(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Lib.Common;;
open Lib.Fragment;;


let main() =
  let all_tabs = [ "orig_links"; "rep_links" ] in
  let editors =
    [ "rep_links", 
        new Custom.Links_ed.editor 
	  "link_editor_rep.cgi" "link_editor_rep" "rep_links" all_tabs;
      "orig_links", 
        new Custom.Links_ed.editor 
	  "link_editor_orig.cgi" "link_editor_orig" "orig_links" all_tabs;
    ]  in
  Custom.Links_ed.switch editors;
  bottom_navigator()
;;

if param "action_add_class" <> "" or param "action_add_class.x" <> "" then begin
  (* Do a redirect *)
  try
    redirect_call_this
      "classifications_editor.cgi"
      [ "edit_page", "select" ]
      [ "name"; "password"; "frames" ]
      "link_editor_rep.cgi"
      "Link Editor"
      [ "action_add_class"; "action_add_class.x"; "action_add_class.y" ]
  with
    any ->
      (* generate error message: *)
      html_header();
      do_protected (fun () -> raise any)
end else begin
  html_header();
  do_protected main
end
;;
