(* $Id$
 * ----------------------------------------------------------------------
 * This script has two parameters:
 * - HTML: A HTML page that may contain a "@AUTO@" parameters
 * - AUTO: automatic parameters that should be passed
 * any other parameters are copied to AUTO.
 *)

open Lib.Common;;
open Lib.Wbuffer;;
open Lib.Fragment;;

let main() =	
  let filename = param "HTML" in

  let frames = (any_param "frames" = "true") in

  List.iter
    (fun name ->
      if name <> "HTML" & name <> "AUTO" then
	set_auto_param name (param name))
    (param_list());

  let head = top_navigator_as_string() in
  let foot = bottom_navigator_as_string() in

  if String.contains filename '/' then (
    do_protected(fun () -> failwith "Forbidden!");
    exit 0;
  );

  include_file filename 
    []
    [ "AUTO",  Verbatim(encoded_auto_params());
      "HEAD",  Verbatim(head);
      "FOOT",  Verbatim(foot);
      "IFFRAMES",         Verbatim(if frames then "" else "<!-- ");
      "END_IFFRAMES",     Verbatim(if frames then "" else " -->");
      "IFNOFRAMES",       Verbatim(if frames then "<!-- " else "");
      "END_IFNOFRAMES",   Verbatim(if frames then " -->" else "");
    ]
;;


html_header();
do_protected main;;
