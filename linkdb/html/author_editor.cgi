(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Lib.Common;;
open Custom.Authors_base;;
open Custom.Links_base;;
open Lib.Wbuffer;;
open Lib.Field;;
open Lib.Fragment;;
open Lib.Scheme;;


class editor =
  object (self)
    inherit Lib.Edit.t "author_editor.cgi" "author_editor" "authors" as super

    val mutable user_id = (-1)

    method authenticate() =
      let name     = any_param "name" in
      let password = any_param "password" in
      set_auto_param "name" name;
      set_auto_param "password" password;
      user_id <- authenticate name password;
      set_auto_param "id" (string_of_int user_id)

    method preaction_change_form_again() =
      failwith "author_editor.cgi: change_form_again not implemented"
    method preaction_add_form_again() =
      failwith "author_editor.cgi: add_form_again not implemented"
    method preaction_change_form() = 
      self # authenticate()
    method preaction_change =      
      self # authenticate
    method preaction_delete() =
      self # authenticate();
      (* Check that there are no links: *)
      let links = Lazy.force !link_db in
      if links # select 
	  (fun r -> dest_int(r # field "owner_id") = user_id ) <> [] then
	raise (User_error "You still own entries in the data base; remove them before deleting the user record")

    method preaction_select() =
      self # authenticate()

    method postaction_change() =
      (* If "password" is null, do not change it *)
      let pw = List.assoc "password" current_record in
      if pw = Lib.Field.null then
	current_record <-
	  ("password", mk_password (any_param "password")) ::
	  List.flatten
	    (List.map
	       (fun (n,v) -> if n = "password" then [] else [n,v])
	       current_record);
      (* Put new identiy into name/password pair *)
      set_auto_param "name" (any_param "att_name");
      if pw <> Lib.Field.null then
	set_auto_param "password" (any_param "att_password")

    method reply page =
      (* add some parameters of common usage *)
      let frames = (any_param "frames" = "true") in
      [ "IFFRAMES",         Verbatim(if frames then "" else "<!-- ");
	"END_IFFRAMES",     Verbatim(if frames then "" else " -->");
	"IFNOFRAMES",       Verbatim(if frames then "<!-- " else "");
	"END_IFNOFRAMES",   Verbatim(if frames then " -->"  else "");
      ]	
      @ super # reply page

    method reply_select() =
      super # reply_select() @
      [ "NAME", Escape_html(any_param "name");
      ]

    method reply_add() =
      set_auto_param "name" (any_param "att_name");
      set_auto_param "password" (any_param "att_password");
      super # reply_add()

    method reply_change() =
      let _ = super # reply_change() in
      raise(Lib.Edit.Goto "select")

    method html_input_size name =
      match name with
	"name"      -> (40,0)
      |	"password"  -> (40,0)
      |	"email"     -> (60,0)
      |	"home_link" -> (60,0)
      | _ -> failwith "...# hmtl_input_size"

  end


let editor = new editor;;

let main() =
  set_auto_param "frames" (any_param "frames");
  editor # invoke();
  bottom_navigator()
;;

html_header();
do_protected main;;
