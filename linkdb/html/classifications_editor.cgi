(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Lib.Common;;
open Lib.Fragment;;


class editor =
  object(self)
    inherit Custom.Cats_ed.editor 
	"classifications_editor.cgi" "classifications_editor" "classifications" 
	as super

    method config_using_tables() =
      [ "orig_links", "classification";
	"rep_links", "classification";
      ]	

    method reply_select() =
      let r = super # reply_select() in
      let protect f x =
	try f x 
	with Not_found -> 
	  failwith "Classifications_editor.editor # reply_select" in

      r @ [ "return_form",   Verbatim(protect return_form());
	    "return_text",   Escape_html(protect caller())
	  ]

  end
;;


let main() =
  (new editor) # invoke();
  bottom_navigator()
;;

html_header();
do_protected main;;
