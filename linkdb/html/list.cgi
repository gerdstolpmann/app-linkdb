(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Lib.Common;;
open Custom.Authors_base;;
open Custom.Links_base;;
open Lib.File_base;;
open Lib.Wbuffer;;
open Lib.Field;;
open Lib.Fragment;;
open Lib.Record;;


(* CGI parameters:
 * "type":             either "title" or "author" or "date"
 *)


type sorted_by = Title | Author | Date;;


let print_rec r =

  let title = field r "title" in
  let author = field r "author_name" in
  let coauthor = field r "coauthor_name" in
  let release = field r "release" in
  let subtitle = field r "subtitle" in
  let info = field r "info_link" in
  let download = field r "download_link" in
  let date = field r "modification_date" in

  let author_link = field r "author_link" in
  let coauthor_link = field r "coauthor_link" in

  let reporter = field r "reporter_name" in

  let is_original = field r "reporter_id" = Lib.Field.null in

  let indent = "<td>&#160;&#160;&#160;&#160;</td>" in

  let ap = encoded_auto_params() in

  let format =
    [ Html "<tr>";
      (if is_original then
         Html "<td rowspan=3 width=30 valign=top><img src=\"icons/clef.gif\" alt=\"original link\" align=top></td>"
       else
         Html "<td rowspan=3 width=30 valign=top><img src=\"icons/dot.gif\" alt=\"reported link\" align=top></td>");

      Html "<td colspan=2><b>";
      Field ("title", title);
      Html "</b> ";

      Html "<font size=-2>";

      (if release <> null then Html "&nbsp;&nbsp;&nbsp;Release " else Html "");
      Field ("release", release);

      Html "&nbsp;&nbsp;&nbsp;";
      Field ("date", date);

      Html "&nbsp;&nbsp;&nbsp;";
      (if is_original then 
	Html "ORIGINAL LINK" 
      else
	Html "");

      Html "</font>";
 
      Html "</td></tr>\n";
      Html "<tr>";
      Html ("<td>");

      Html "\n";
      Field ("subtitle", subtitle);

      Html "\n - written by ";
      if author=null then
	Html "anonymous"
      else
	Field ("author", author);

      begin
	if coauthor <> null then
          List ("",[ Html "\nand "; 
                     Field ("coauthor", coauthor);
		   ])
	else
          Html ""
      end;

      (if not is_original then
	 List ("", [ 
	   Html "; reported by ";
	   Field ("reporter", reporter);
	   Html ". "
	 ])
       else
	 Html ". ");

      Html ("<a href=\"link_editor_orig.cgi?edit_page=show&id=" ^ string_of_int (r#id()) ^ 
	    "&AUTO=" ^ ap ^ "\">");
      Html "Details</a>\n";
      Field ("info_link", info);
      Html "\n";
      Field ("download_link", download);
      Html "\n";
      Field ("author_link", author_link);
      Html "\n";
      Field ("coauthor_link", coauthor_link);

      Html "</td></tr>\n";
      Html "<tr><td height=8></td></tr>\n";
    ] in

  let config =
    [ "info_link",     Out_hyperlink "Info";
      "download_link", Out_hyperlink "Download";
      "author_link",   Out_hyperlink "Author";
      "coauthor_link", Out_hyperlink "Co-Author";
    ] in
       
  print (html_output [ Config(config, format) ])
;;



let main() =
  let type_param = param "type" in

  set_auto_param "frames" (any_param "frames");

  let split_in_sections t crit disp disp_null lst =
    let rec split last_crit last_disp deferred lst =
      match lst with
	[] -> 
	  if deferred <> [] then [ last_disp, List.rev deferred ] else []
      |	r :: lst' ->
	  let this = r # field crit in
	  if Lib.Field.compare_fields t this last_crit = 0 then
	    split last_crit last_disp (r :: deferred) lst'
	  else
	    let this_disp = 
	      if this = null then
		disp_null
	      else
		html_output [ Field("noname", r # field disp) ] in
	    if deferred = [] then
	      split this this_disp [r] lst'
	    else
	      (last_disp, List.rev deferred) ::
	      split this this_disp [r] lst'
    in
    split null disp_null [] lst
  in

  let lst = 
    match type_param with
      "author" ->
	let links = author_title_links () in
	split_in_sections 
	  T_line "author_name" "author_name" "Anonymous" links
    | "title" ->
	[ "", title_author_links ()]
    | "date" ->
	split_in_sections
	  T_date "modification_date" "modification_date" "Unknown" 
	  (date_author_links ())
(*    | "classification" ->
	split_in_sections
	  (T_select (Classifications_base.selection))
	  "classification" "classification" "Unclassified"
	  (classification_author_links())
*)
    | "classification" ->
	classified_links()
    | _ ->
	failwith "This type of sorting is not implemented"
  in
      
  print "<html>\n";
  print ("<head><title>The links, sorted by " ^ type_param ^ " </title></head>\n");
  print "<body bgcolor=white text=black link=black alink=black vlink=\"#99A1C5\">\n";
  top_navigator();
  List.iter 
    (fun (header, items) ->
      if header <> "" then begin
	print "<table cellspacing=0 border=0 bgcolor=\"#B9C1E5\" width=\"100%\">\n";
	print "<tr><td align=left><img src=\"icons/empty.gif\" width=30 height=1>";
	print "<font size=4><b>";
	print header;
	print "</b></font>";
	print "</td></tr>";
	print "</table>\n";
      end;
      print "<table cellspacing=0 border=0>\n";
      List.iter print_rec items;
      print "</table>\n";
    )
    lst;
  bottom_navigator();
  print "</body>\n";
  print "</html>\n";
;;

(*
Logging.loglevels :=  [ Logging.Debug; Logging.Param; Logging.Info; 
  Logging.Warn; Logging.Err ];;
*)

html_header();
do_protected main;;
