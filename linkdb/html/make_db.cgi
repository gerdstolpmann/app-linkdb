(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Lib.Common;;

print_string "Content-type: text/plain\n\n"
;;

List.iter
  (fun n ->
    Sys.command("touch " ^ !basedir ^ "/" ^ n); ())
  [ "authors.db";
    "classifications.db";
    "orig_links.db";
    "rep_links.db";
  ];

let f = !basedir ^ "/index.db" in
let _ = Sys.command ("test -f " ^ f ^ " || echo 0 >" ^ f) in

print_string "Ok\n"
;;
