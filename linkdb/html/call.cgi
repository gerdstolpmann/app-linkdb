(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* This script does a redirect to call subprograms. 
 * See Common.redirect_call
 *)

open Lib.Common;;
open Lib.Fragment;;

try
  redirect_call()
with
  any ->
    (* generate error message: *)
    html_header();
    do_protected (fun () -> raise any)
;;
