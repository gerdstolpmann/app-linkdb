(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Lib.Common;;

(* Define a catalogue. *)

let scheme =
  Lib.Scheme.mk_scheme
    [ "title", Lib.Field.T_constrained
	         (Lib.Field.T_line,  (* Must be T_line in a catalogue *)
		  (fun f -> if f = Lib.Field.null then
		    raise(User_error "Field 'title' must not be empty!")));
      "id",    Lib.Field.T_line;  (* Must be T_line in a catalogue *)
    ] 
    [ "title" ]
    [ "id" ]
;;


class classifications =
  object (self)
    inherit Lib.File_base.t scheme  "classifications.db" "classifications" as super


    method get_derived_field r n =
      match n with
	"id" ->
	  Lib.Field.mk_line(string_of_int(r # id()))
      |	_ ->
	  failwith ("Classifications_base: unknown derived field " ^ n)

    method add_record fields =
      try
        super # add_record fields 
      with
        Lib.Table.Not_unique "title" ->
	  raise (User_error "This title has already been entered")


    method change_record id fields =
      try
        super # change_record id fields 
      with
        Lib.Table.Not_unique "title" ->
	  raise (User_error "This title has already been entered")

  end
;;

Lib.Cgi.register_init
  (fun () ->
     Lib.Base.register_table 
       "classifications" 
       (new classifications :> Lib.Table.t);

     Lib.Base.register_catalogue_exd
       "classifications"
       "classifications"
       "id"
       "title"
       true
       []
  )
;;

let selection() = Lib.Base.get_selection "classifications"
;;
