(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Lib.Common;;
open Authors_base;;
open Links_base;;
open Buffer;;
open Lib.Field;;
open Lib.Fragment;;
open Lib.Scheme;;

class editor url basename table_name =
  object(self) 
    inherit Lib.Edit.t url basename table_name as super

    val mutable user_id = (-1)

    method authenticate() =
      let name     = any_param "name" in
      let password = any_param "password" in
      set_auto_param "name" name;
      set_auto_param "password" password;
      user_id <- authenticate name password

    method check_id() =
      if int_of_string(any_param "id") < 0 then
	raise(User_error "No real entry selected")

    (* all actions require authentication: *)

    method preaction_add_form_again() =
      failwith "Cats_ed # editor: add_form_again not implemented"
    method preaction_change_form_again() =
      failwith "Cats_ed # editor: change_form_again not implemented"
    method preaction_add_form =    self # authenticate
    method preaction_add =         self # authenticate
    method preaction_change_form() = 
      failwith "Cats_ed # editor: change not implemented"
    method preaction_change = 
      failwith "Cats_ed # editor: change not implemented"
    method preaction_delete() =
      self # authenticate();
      self # check_id();
      let id = any_param "id" in
      let to_check = self # config_using_tables() in
      List.iter
	(fun (tab_name, att_name) ->
	  let tab = 
	    try Lib.Base.get_table tab_name 
	    with Not_found -> failwith "Cats_ed.editor # preaction_delete" in
	  if tab # select 
	      (fun r -> 
		try
		  List.mem id (dest_select(r # field att_name))
		with
		  No_content -> false     (* raised by 'dest_select' *)
		| Not_found ->            (* raised by '#field' *)
		    failwith "Cats_ed.editor # preaction_delete") <> [] 
	  then
	    raise(User_error "There is still a record using this element"))
	to_check;
      ()

    method preaction_select =      self # authenticate


    method config_using_tables() =
      (* If there is a record that uses the element to be deleted, 
       * the deletion is not possible. You can configure here
       * a list of (table_name, field_name) pairs that are to be
       * checked.
       *)
      []


    method reply page =
      (* add some parameters of common usage *)
      let frames = (any_param "frames" = "true") in
      [ "IFFRAMES",         Verbatim(if frames then "" else "<!-- ");
	"END_IFFRAMES",     Verbatim(if frames then "" else " -->");
	"IFNOFRAMES",       Verbatim(if frames then "<!-- " else "");
	"END_IFNOFRAMES",   Verbatim(if frames then " -->"  else "");
      ]	
      @ super # reply page

    method reply_select() =
      (* customize the output of the "select" page *)
      let r =
	(* add "NAME", the name of the user: *)
	super # reply_select() @ [ "NAME", Escape_html(any_param "name") ]
      in

      (* add "NOBUTTON_L" and "_R" which are set to comment parantheses
       * if there is no link in the database to select. This causes that
       * anything between "NOBUTTON_L" and "_R" is suppressed. It is
       * intended to hide the "submit" button.
       *)
      if selection_list = [] then
	r @ [ "NOBUTTON_L", Verbatim "<!-- ";
	      "NOBUTTON_R", Verbatim " -->" ]
      else
	r @ [ "NOBUTTON_L", Verbatim "";
	      "NOBUTTON_R", Verbatim "" ]

    method msg_select_id rec_id =
      (* customize the output of the "select" page *)
      let record  = List.assoc rec_id selection_list in
      let title   = record # field "title" in
      
      dest_line title

    method reply_add() =
      let _ = super # reply_add() in
      raise (Lib.Edit.Goto "select")

    method reply_delete() =
      let _ = super # reply_delete() in
      raise (Lib.Edit.Goto "select")
  end
;;
