(* $Id$
 * ----------------------------------------------------------------------
 *
 *)


Lib.Cgi.register_init
  (fun () ->
     Lib.Base.register_catalogue_directly
       "stability"
       [ "dev",     ("development code", false);
	 "alpha",   ("alpha-test code", false);
	 "beta",    ("beta-test code", false);
	 "stable",  ("stable code", false);
	 "unknown", ("unknown stability", true)
       ] 
       false
  )
;;


let stability() = Lib.Base.get_selection "stability";;
