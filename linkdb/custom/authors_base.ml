(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* Authors_base:
 *
 * A class that is derived from File_base.t and handles the specific
 * issues of the author database.
 *)

(* Attributes of an 'author':
 * "name"       Full name
 * "email"      Email address x@y.z
 * "home_link"  An href pointing to information about the author
 * "password"   The password
 *)

open Lib.Common

let scheme =
  Lib.Scheme.mk_scheme 
    [ "name",      Lib.Field.T_constrained
	             (Lib.Field.T_line,
		      (fun f -> if f = Lib.Field.null then
			raise(User_error "Field 'Name' must not be empty")));
      "email",     Lib.Field.T_line;
      "home_link", Lib.Field.T_hyperlink;
      "password",  Lib.Field.T_password;
    ] 
    [ "name" ]
    []
;;


(**********************************************************************)
(* Authors_base.t: 
 * Persistent author database. Compared with File_base.t, some checks on
 * the integrity have been added.
 *)

class authors =
  object (self)
    inherit Lib.File_base.t scheme "authors.db" "authors" as super


    method check_type fields =
      super # check_type fields;
      let pw = List.assoc "password" fields in
      if pw = Lib.Field.null then
	raise(User_error "You must have a password!")

    method add_record fields =
      try
	super # add_record fields 
      with
	Lib.Table.Not_unique "name" ->
	  raise (User_error "A user with this name already exists!");


    method change_record id fields =
      try
	super # change_record id fields 
      with
	Lib.Table.Not_unique "name" ->
	  raise (User_error "A user with this name already exists!");

  end
;;

(**********************************************************************)

Lib.Cgi.register_init
  (fun () ->
     Lib.Base.register_table "authors" (new authors :> Lib.Table.t));;


(* As we have only ONE author DB, there should be a global variable that
 * contains that DB. The following functions simplify the access to the
 * DB.
 *)

let authenticate name password =
  (* Authenticate an author by name and password. Returns the ID of the
   * author or fails
   *)
  Lib.Lock.require_read_permission();
  let db = Lib.Base.get_table "authors" in
  let exists r = 
    (Lib.Field.mk_line name = Lib.Record.field r "name") & 
    (Lib.Field.eq_password (Lib.Record.field r "password") password)
  in
  let s = db # select exists in
  if s = [] then
    raise (User_error "Login failed!");
  Lib.Record.id (List.hd s)
;;


let about_author id =
  (* Get the record for the author with ID 'id' *)
  let db = Lib.Base.get_table "authors" in
  db # get_record id
;;
