(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* Links_base:
 *
 * A class that is derived from File_base.t and handles the specific
 * issues of the link database.
 * Currently, there are two tables storing links: orig_links and rep_links
 * for originial links resp. reported links.
 *)

open Lib.Common
open Lib.File_base
open Str
open Lib.Record
open Lib.Table



(* Fields for both orig_links and rep_links: *)

let common_fields() =
  [ "title",                Lib.Field.T_constrained
                                (Lib.Field.T_line,
				 (fun f -> if f = Lib.Field.null then
				   raise(User_error "Field 'title' must not be empty")));
    "subtitle",             Lib.Field.T_constrained
	                        (Lib.Field.T_line,
				 (fun f -> if f = Lib.Field.null then
				   raise(User_error "Field 'subtitle' must not be empty")));
    "owner_id",             Lib.Field.T_int;
    "info_link",            Lib.Field.T_hyperlink;
    "download_link",        Lib.Field.T_hyperlink;
    "modification_date",    Lib.Field.T_date;
    "classification",       Lib.Field.T_select (Classifications_base.selection());
    "release",              Lib.Field.T_line;
    "stability",            Lib.Field.T_select (Cats.stability());
    "stable_release",       Lib.Field.T_line;
    "license",              Lib.Field.T_line;
    "description",          Lib.Field.T_textbox;
    "recent_changes",       Lib.Field.T_textbox;
    "dep_ocaml",            Lib.Field.T_line;
    "dep_os",               Lib.Field.T_line;
    "dep_software",         Lib.Field.T_textbox;
  ] 
;;


let orig_scheme() =
  Lib.Scheme.mk_scheme 
    (common_fields() @
     [ "author_id",            Lib.Field.T_int;
       "author_name",          Lib.Field.T_line;
       "author_link",          Lib.Field.T_hyperlink;
       "coauthor_name",       Lib.Field.T_line;
       "coauthor_link",       Lib.Field.T_hyperlink;
     ]) 
    []
    [ "author_id";
      "author_name";
      "author_link"
    ]
;;


let rep_scheme() =
  Lib.Scheme.mk_scheme
    (common_fields() @
     [ "reporter_id",     Lib.Field.T_int;
       "reporter_name",   Lib.Field.T_line;
       "author_name",     Lib.Field.T_line;
       "author_link",     Lib.Field.T_hyperlink;
       "coauthor_name",  Lib.Field.T_line;
       "coauthor_link",  Lib.Field.T_hyperlink;
     ])
    []
    [ "reporter_id";
      "reporter_name" ]
;;


(**********************************************************************)
(* Links_base.t: 
 * Persistent link database. Compared with File_base.t, some checks on
 * the integrity have been added. Furthermore, the derived attribute
 * "author_name" is added.
 *)

class links scheme name =
  object (self)
    inherit Lib.File_base.t scheme (name ^ ".db") name as super


    method get_derived_field r n =
      let lookup attribute id =
	(try
	  let author_id = Lib.Field.dest_int id in
	  let author_rec = Authors_base.about_author author_id in
	  field author_rec attribute
	with 
	  Lib.Field.No_content -> Lib.Field.null)
      in
      match n with
	"author_name"    -> lookup "name" (field r "owner_id")
      |	"reporter_name"  -> lookup "name" (field r "owner_id")
      |	"author_link"    -> lookup "home_link" (field r "owner_id")
      |	"author_id"      -> r # field "owner_id"
      |	"reporter_id"    -> r # field "owner_id"
      |	_ ->
	  failwith ("Links_base.t: unknown derived field " ^ n)

  end
;;

(**********************************************************************)

(* Now the access functions for everybody: *)

Lib.Cgi.register_init
  (fun () ->
     Lib.Base.register_table 
       "orig_links" 
       (new links (orig_scheme()) "orig_links" :> Lib.Table.t);

     Lib.Base.register_table 
       "rep_links" 
       (new links (rep_scheme()) "rep_links" :> Lib.Table.t)
  )
;;


(************ The following currently only for orig_links ****************)


let get_link_db() =
  let orig_links = Lib.Base.get_table "orig_links" in
  let rep_links  = Lib.Base.get_table "rep_links" in
  new Lib.View.intersection [orig_links; rep_links]

let link_db = ref(lazy(get_link_db()))
;;

Lib.Cgi.register_init
  (fun () -> link_db := lazy(get_link_db())) ;;


let release_re = regexp "^[0-9]+\\(\\.[0-9]+\\)$";;
let release_split_re = regexp "\\.";;

let cmp_release a_field b_field =
  (* Compare two release numbers.
   * Returns negative number if b_field is smaller.
   * Return positive number if a_field is smaller.
   * Returns 0 if both releases are identical.
   * Note: This makes that release numbers are sorted in decending order!
   *)
  try
    let a = Lib.Field.dest_line a_field in   (* or raise No_content *)
    let b = Lib.Field.dest_line b_field in   (* or raise No_content *)
    if string_match release_re a 0 & string_match release_re b 0 then begin
      let a_list = List.map int_of_string (split release_split_re a) in
      let b_list = List.map int_of_string (split release_split_re b) in
      List.fold_left2
	(fun r ak bk -> if r = 0 then -(compare ak bk) else r)
	0
	a_list
	b_list
    end else
      -(compare a b)  (* generic comparison *)
  with
      Lib.Field.No_content ->
	(* One or both fields are empty. In this case, the empty field
	 * is smaller than the filled field.
	 *)
	if a_field = Lib.Field.null then begin
	  if b_field = Lib.Field.null then
	    0
	  else
	    1
	end
	else (-1)
;;


let title_author_links () =
  (* get links sorted first by title, then by release, then by author *)
  let db = Lazy.force !link_db in
  db # get_somehow_sorted_records 
    ["title", Lib.Field.compare_fields Lib.Field.T_line; 
      "release", cmp_release; 
      "author_name", Lib.Field.compare_fields Lib.Field.T_line]
;;


let author_title_links () =
  (* get links sorted first by author, then by title, then by release *)
  let db = Lazy.force !link_db in
  db # get_somehow_sorted_records 
    ["author_name",Lib. Field.compare_fields Lib.Field.T_line; 
      "title", Lib.Field.compare_fields Lib.Field.T_line; 
      "release", cmp_release]
;;


let date_author_links () =
  (* get links sorted by date, then by title, then by release, then by author *)
  let db = Lazy.force !link_db in
  db # get_somehow_sorted_records 
    [ "modification_date", 
         (fun a b -> 
	   if a = Lib.Field.null & b = Lib.Field.null then 0 else
	   if a = Lib.Field.null then 1 else
	   if b = Lib.Field.null then (-1) else
	   Lib.Field.compare_fields Lib.Field.T_date b a);
      "title", Lib.Field.compare_fields Lib.Field.T_line; 
      "release", cmp_release;
      "author_name", Lib.Field.compare_fields Lib.Field.T_line; 
    ]
;;


let classification_author_links () =
  (* get links sorted by classification, then by title, then by release, then by author *)
  let db = Lazy.force !link_db in
  db # get_somehow_sorted_records 
    [ "classification", Lib.Field.compare_fields 
	                  (Lib.Field.T_select (Classifications_base.selection()));
      "title", Lib.Field.compare_fields Lib.Field.T_line; 
      "release", cmp_release;
      "author_name", Lib.Field.compare_fields Lib.Field.T_line; 
    ]
;;


let classified_links () =
  (* get links sorted by classification; links may be given several times
   * per classification
   *)
  let db = Lazy.force !link_db in
  let cl_tab = Lib.Base.get_table "classifications" in
  let cl_lst = cl_tab # select (fun _ -> true) in
  let base_lst = title_author_links() in
  let l =
    List.map
      (fun cl ->
	let cl_val = Lib.Field.dest_line (cl # field "id") in
	Lib.Field.html_output [Lib.Field.Field("noname", cl # field "title")],
	List.filter
	  (fun r ->
	     try
	      let f = r # field "classification" in
	      List.mem cl_val (Lib.Field.dest_select f)
	    with
	      (Lib.Field.No_content|Not_found) -> false)
	  base_lst)
      cl_lst
  in
  Sort.list (fun a b -> String.lowercase(fst a) <= String.lowercase(fst b)) l
;;


let author's_links id =
  (* get all links for the author with ID 'id' sorted first by title,
   * then by release
   *)
  let str_id = Lib.Field.mk_int id in
  let db = Lazy.force !link_db in
  let recs = db # get_sorted_records ["title"; "release"] in
  List.flatten
    (List.map
       (fun r ->
	 if field r "author_id" = str_id then [ r ] else [])
       recs)
;;



(* TODO: The following functions are deprecated. Remove them. *)

let new_link fields =
  (* add new link to the database *)
  let db = Lazy.force !link_db in
  let _ = db # add_record fields in
  db # write_database ()
;;


let change_link id fields =
  (* change link with 'id' to value 'fields' *)
  let db = Lazy.force !link_db in
  db # change_record id fields;
  db # write_database ()
;;


let delete_link id =
  (* delete link with ID 'id' *)
  let db = Lazy.force !link_db in
  db # delete_record id;
  db # write_database()
;;


let get_link id =
  (* returns record or raises Not_found *)
  let db = Lazy.force !link_db in
  db # get_record id
;;
