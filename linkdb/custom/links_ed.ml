(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Lib.Common;;
open Authors_base;;
open Links_base;;
open Buffer;;
open Lib.Field;;
open Lib.Fragment;;
open Lib.Scheme;;


class editor url the_basename table_name selection_tables =
  object (self)
    inherit Lib.Edit.t url "link_editor" table_name as super

    val sel_tables = selection_tables
    val local_basename = the_basename
    val mutable user_id = (-1)


   method invoke() =
      if any_param "ok" <> "" or any_param "ok.x" <> "" then begin
	match edit_page with
	  "change_form_again" -> edit_page <- "change"
	| "add_form_again"    -> edit_page <- "add"
	| _                   -> ()
      end;
      super # invoke()


    method authenticate() =
      let name     = any_param "name" in
      let password = any_param "password" in
      set_auto_param "name" name;
      set_auto_param "password" password;
      user_id <- authenticate name password

    method check_id() =
      if int_of_string(any_param "id") < 0 then
	raise(User_error "No real entry selected")

    (* all actions require authentication: *)

    method preaction_add_form =    
      self # authenticate
    method preaction_add_form_again =    
      self # authenticate
    method preaction_add =         
      self # authenticate
    method preaction_change_form() = 
      self # authenticate();
      self # check_id()
    method preaction_change_form_again() = 
      self # authenticate();
      self # check_id()
    method preaction_change =      
      self # authenticate
    method preaction_delete() =
      self # authenticate();
      self # check_id()
    method preaction_select =      
      self # authenticate


    method action_select() =
      (* select only entries owned by user_id *)
      selection_list <- 
	List.flatten
	  (List.map
	     (fun tab_name ->
	       let tab = Lib.Base.get_table tab_name in
	       List.map 
		 (fun r -> r # id(), r) 
		 (tab # select 
		    (fun r -> dest_int(r # field "owner_id") = user_id )))
	     sel_tables)


    method action_add() =
      (* add "owner_id" and "modification_date" *)
      super # action_add();
      current_record <-
	[ "owner_id",          mk_int user_id;
	  "modification_date", mk_today()
	] 
	@ current_record


    method action_change() =
      (* add "owner_id" and "modification_date" *)
      super # action_change();
      current_record <-
	[ "owner_id",          mk_int user_id;
	  "modification_date", mk_today()
	] 
	@ current_record


    method postaction_change_form() =
      (* store "current_id" *)
      set_auto_param "id" (string_of_int current_id)


    method postaction_change_form_again() =
      (* store "current_id" *)
      set_auto_param "id" (string_of_int current_id)


    method reply page =
      (* add some parameters of common usage *)
      let frames = (any_param "frames" = "true") in
      [ "IFFRAMES",         Verbatim(if frames then "" else "<!-- ");
	"END_IFFRAMES",     Verbatim(if frames then "" else " -->");
	"IFNOFRAMES",       Verbatim(if frames then "<!-- " else "");
	"END_IFNOFRAMES",   Verbatim(if frames then " -->"  else "");
      ]	
      @ super # reply page


    method reply_select() =
      (* customize the output of the "select" page *)
      let r =
	(* add "NAME", the name of the user: *)
	super # reply_select() @ [ "NAME", Escape_html(any_param "name") ]
      in

      (* add "NOBUTTON_L" and "_R" which are set to comment parantheses
       * if there is no link in the database to select. This causes that
       * anything between "NOBUTTON_L" and "_R" is suppressed. It is
       * intended to hide the "submit" button.
       *)
      if selection_list = [] then
	r @ [ "NOBUTTON_L", Verbatim "<!-- ";
	      "NOBUTTON_R", Verbatim " -->" ]
      else
	r @ [ "NOBUTTON_L", Verbatim "";
	      "NOBUTTON_R", Verbatim "" ]


    method msg_select_id rec_id =
      (* customize the output of the "select" page *)
      let record  = List.assoc rec_id selection_list in
      let title   = record # field "title" in
      let release = record # field "release" in
      
      dest_line title ^ 
      (if release = null then "" else ", release " ^ dest_line release)


    method add_extras() =
      (* customize the output of the "add_form" page *)
      [ "TITLE",   Escape_html "Add new entry";
	"NEXT",    Escape_html "add_form_again";
	"SUBMIT",  Escape_html "Add this link";
	"LABEL",   Escape_html "add-this";
	"ro_author_name", Escape_html(any_param "name");
	"ro_reporter_name", Escape_html(any_param "name")
      ]	

    method reply_add_form() =
      self # add_extras()
      @ super # reply_add_form()

    method reply_add_form_again() =
      self # add_extras()
      @ super # reply_add_form_again()

    method reply_add() =
      let super's_answer = super # reply_add() in
      [ "RECORD_AS_TABLE", 
	Verbatim(include_file_as_string
		   (local_basename ^ ".record.fhtml")
		   (self # default_config current_record)
		   (self # reply_output_form()));
	"id", Escape_html(string_of_int current_id);
      ]	
      @ super's_answer

    method reply_show() =
      let super's_answer = super # reply_show() in
      super's_answer @
      [ "RECORD_AS_TABLE", 
	Verbatim(include_file_as_string
		   (local_basename ^ ".record.fhtml")
		   (self # default_config current_record)
		   (self # reply_output_form()));
	"id", Escape_html(string_of_int current_id)
      ]	

    method change_extras() =
      (* customize the output of the "change_form" page *)
      [ "TITLE",   Escape_html "Change entry";
	"NEXT",    Escape_html "change_form_again";
	"SUBMIT",  Escape_html "Change this link";
	"LABEL",   Escape_html "change-this";
      ]	

    method reply_change_form() =
      super # reply_change_form() @
      self # change_extras()

    method reply_change_form_again() =
      super # reply_change_form_again() @
      self # change_extras()

    method reply_change() =
      let _ = super # reply_change() in
      raise(Lib.Edit.Goto "select")

    method reply_delete() =
      let _ = super # reply_delete() in
      raise(Lib.Edit.Goto "select")


    (* Use the same fragment file for "add" and "change" requests: *)

    method fragment_file_add_form    = local_basename ^ ".form.fhtml"
    method fragment_file_change_form = local_basename ^ ".form.fhtml"

    (* customize layout of input forms: *)

    method html_input_size name =
      match name with
	"title"         -> (40,0)
      |	"subtitle"      -> (60,0)
      |	"release"       -> (20,0)
      |	"info_link"     -> (60,0)
      |	"download_link" -> (60,0)
      |	_ -> (60,5)

  end
;;


let switch editors =
  (* editors: alist with keys = table names, values = editor objects.
   * If there is an "id" parameter, a table lookup is done and the
   * appropriate editor is chosen.
   * Otherwise, the first editor from 'editors' is used.
   *)

  let d = snd(List.hd editors) in
  (* If there is an "id" parameter, do a table lookup *)
  let page = any_param "edit_page" in
  let need_id = page <> "add" & page <> "add_form" & page <> "select" in
  let id_p = any_param "id" in
  if need_id & id_p <> "" then begin
    let id = int_of_string id_p in
    if id < 0 then
      raise(User_error "No real entry selected");
    Lib.Lock.with_read_permission
      (fun () ->
	let tab_name = 
	  try Lib.Index.lookup_id id with Not_found -> failwith "Links_ed.switch" in
	let ed =
	  try List.assoc tab_name editors 
	  with Not_found -> failwith "Links_ed.switch" in
	ed # invoke())
      ()
  end
  else
    d # invoke()
;;
