(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Field
open Common
open Scheme
open Fragment

class t the_scheme =
  object (self)

    val scheme = the_scheme

    method html_display fragment r =
      include_file
	fragment
	[]
	(parameters_of_record scheme r);



    method html_input_form_spec (javascript:bool) 
                                (r : (string * Field.field) list) =
      
      let field n =
	try
	  List.assoc n r 
	with
	  Not_found -> Field.null
      in

      let vals =
	List.map
	  (fun (n,t) ->
	    let (w,h) = self # html_input_size n in
	    (* Logging.write ("form " ^ n); *)
	    let n' = "att_" ^ n in
	    n',
	    Verbatim
	      (match get_base_type t with
		T_line       -> html_input_line n' w (field n)
	      | T_password   -> html_input_password n' w (field n)
	      |	T_textbox    -> html_input_textbox n' w h (field n)
	      | T_hyperlink  -> html_input_hyperlink n' javascript w (field n)
	      | T_int        -> html_input_int n' javascript w (field n)
	      | T_date       -> html_input_date n' javascript w (field n)
	      |	T_constrained (_,_) -> failwith "internal error"
	      | T_select sel -> 
		  let rv = field n in
		  let sv =
		    if rv = null then
		      mk_select_default t
		    else
		      rv in
		  html_input_select_asbox n' h sv))
	  (physical_typedef_of_scheme scheme)
      in
      vals



    method html_input_form fragment javascript r =
      include_file
	fragment
	[]
	(self # html_input_form_spec javascript r)


    method html_input_size name =
      (60,5)
  end
