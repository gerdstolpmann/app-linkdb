(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Scheme

class type record_type =
  object
    method id : unit -> int
    method field : string -> Field.field
    method setrec : (string * Field.field) list -> unit
  end


class type table_type =
  object
    method get_derived_field : record_type -> string -> Field.field
  end


class record new_table new_scheme new_id new_fields =
  object (self)
    val table = (new_table :> table_type)
    val scheme = new_scheme
    val id = (new_id : int)
    val mutable fields = ([] : (string * Field.field Lazy.t) list)

    initializer
      self # setrec new_fields

    method id() = id

    method field name =
      let v =
	try
	  List.assoc name fields
	with
	  Not_found -> lazy (Field.null)
      in
      Lazy.force v

    method setrec f =
      Scheme.check_type scheme f;
      let new_fields =
	List.map
	  (fun (n,t) ->
	    try
	      let v = List.assoc n f in
	      (n,lazy v)
	    with Not_found ->
	      (n,lazy(new_table # get_derived_field (self :> record_type) n)))
	  (typedef_of_scheme scheme)
      in
      fields <- new_fields

  end


(* Compatibility with old software: *)

let field r name = r # field name;;
let id r = r # id();;
