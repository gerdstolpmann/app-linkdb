(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

exception Not_unique of string
  (* Raised if adding or changing a record violates the uniqueness of
   * the attribute given as argument
   *)


class virtual t =
  object (self) 

    method virtual scheme : Scheme.scheme

    method virtual read_database : unit -> unit
    method virtual write_database : unit -> unit

    method virtual begin_transaction : unit -> unit
    method virtual commit_transaction : unit -> unit
    method virtual is_modified : unit -> bool

    method virtual get_derived_field : 
	Record.record_type -> string -> Field.field

    method virtual add_record : (string * Field.field) list -> int
    method virtual change_record : int -> (string * Field.field) list -> unit
    method virtual delete_record : int -> unit

    method virtual get_record : int -> Record.record
    method virtual get_sorted_records : string list -> Record.record list
    method virtual get_somehow_sorted_records :
	(string * (Field.field -> Field.field -> int)) list ->
	  Record.record list

    method virtual select : (Record.record -> bool) -> Record.record list
  end
