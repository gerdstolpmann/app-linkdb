(* $Id$
 * ----------------------------------------------------------------------
 * The subclassable editor for records
 *)


(* This editor class offers the following pages:
 *
 * - select: a form with a selection list of records to edit
 * - add_form: a form to add a new record
 * - add: the action behind add_form
 * - change_form: a form to change an existing record
 * - change: the action behind change_form
 * - delete: delete a known record
 * - show: show the contents of a record
 *
 * The variants "add_form_again" and "change_form_again" are the same
 * forms as "add_form" resp. "change_form", but user input from the last
 * invocation is respected.
 *
 * CGI PARAMETERS:
 *
 * The CGI parameter "edit_page" selects one of the pages. Most pages
 * require that more parameters are given, the following list is the
 * minimum (subclasses may require more parameters):
 *
 * - select: no more parameters required. Normally, a subclass
 *   restricts the records which can be selected and requires more
 *   parameters to do this
 * - add_form: no parameters required.
 *   Optional:
 *   - BACK_INPUTS: initial values for the form
 * - add: the parameters that result from an "add_form" posting must
 *   be there. These are:
 *   - att_<name> where <name> is the name of the attribute: the value
 *     of the attribute <name>
 *   Modifier:
 *   - option_semantics: if given (with arbitrary value) the opt_* 
 *     parameters are evaluated
 *   - opt_<name>: If given, this means that att_<name> contains a valid
 *     value; otherwise att_<name> is ignored
 * - add_form_again: All the parameters described for "add" and "add_form"
 *   can be processed. Note that "BACK_INPUTS" has precedence over the 
 *   values given by "att_<name>.
 * - change_form:
 *   - id: the ID of the record to edit
 *   Optional:
 *   - BACK_INPUTS: initial values for the form
 * - change:
 *   - id: the ID of the changed record
 *   - att_<name>: see "add"
 *   - opt_<name>: see "add"
 *   - option_semantics: see "add"
 * - change_form_again: All the parameters described for "change" and 
 *   "change_form" can be processed. Note that "BACK_INPUTS" has precedence 
 *   over the values given by "att_<name>.
 * - delete:
 *   - id: the ID of the record to be deleted
 * - show:
 *   - id: the ID of the record to show
 *
 * THE ABSTRACT FUNCTIONAL PATTERN
 *
 * For every page, there are usually the following methods (here <page>
 * is a placeholder for the page name, such as "change") which are
 * invoked in turn to process the incoming data and generate HTML output:
 *
 * - preaction_<page>:
 *     - this is an empty method that is intended to be overridden in a
 *       subclass. Example: authentication
 *     - it is invoked right after the page has been called
 * - action_<page>:
 *     - loads the record or finds out its ID
 *     - this method is invoked after preaction_<page>
 * - postaction_<page>:
 *     - this is an empty method that is intended to be overridden in a
 *       subclass. Example: normalization, computing derived fields
 *     - this method is invoked after action_<page>
 * - reply_<page>:
 *     - "add", "change", "delete": modifies the table as requested
 *     - this method generates the HTML text that is the output of the
 *       program.
 *     - this method is invoked after postaction_<page>
 *
 * Additionally, the following methods help to do the job:
 *
 * - fragment_file_<page>
 *     - the file name of the fragment to use for <page>. This is a
 *       constant
 * - default_config:
 *     - the out_config list that is used
 *)


open Common
open Fragment
open Field
open Scheme
open Record

let get_id() =
  try int_of_string (any_param "id")
  with _ -> failwith "not an integer value in CGI parameter 'id'"


exception Goto of string
  (* Goto page: Forces to continue with <page> *)

class t the_url the_basename the_table_name =
  let the_table = Base.get_table the_table_name in
  object (self)
    inherit Layout.t (the_table # scheme)

    val url    = (the_url : string)
    val basename = (the_basename : string)
    val table  = (the_table : Table.t)
    val scheme = the_table # scheme

    val mutable edit_page = any_param "edit_page"
	(* may be changed in subclasses *)

    (* The following instance variables are used to store the record to be
     * edited and its ID. (The latter is (-1) is unknown.)
     *)

    val mutable current_record = []
	(* contains BOTH physical and derived fields. Please note that
	 * the latter are sometimes problematic, as there is no mechanism
	 * to automatically update them if needed.
	 *)
    val mutable current_id = (-1)
   
    val mutable tags = []

    (* The following instance variable contains all records that can be
     * selected
     *)

    val mutable selection_list = []

    val mutable transaction = false

    method invoke() =
      Lock.with_read_permission (self # invoke_lck) ()

    method invoke_lck () =
      Logging.write Logging.Debug "Edit.invoke";
      let again = ref true in
      let page = ref edit_page in
      while !again do
	begin match !page with
	  ("add" | "add_form" | "select") ->
	    unset_auto_param "id"
	| _ -> ()
	end;
	try
	  again := false;
	  if self # need_write_lock !page then begin
	    if not transaction then begin
	      Lock.get_write_lock();
	      Base.begin_transaction();
	      transaction <- true;
	    end
	  end;
	  self # preaction !page;
	  self # action !page;
	  self # postaction !page;
	  let reply_parameters = self # reply !page in
	  let file = self # fragment_file !page in
	  include_file
	    file
	    (self # default_config current_record)
	    (reply_parameters @
	     [ "AUTO", Verbatim(encoded_auto_params());
	       "BACK", Escape_html(url);
	       "BACK_PARAMS", Verbatim(back_params ["edit_page", !page]);
	     ])
	with
	  Goto page' ->
	    again := true;
	    page := page'
      done;
      if transaction then
	Base.commit_transaction();
      transaction <- false


    method fetch_record() =
      (* override 'current_record' from CGI parameters *)
      let option_semantics = any_param "option_semantics" <> "" in
      (* first set 'BACK_INPUTS' (this is fail-safe) *)
      List.iter
	(fun (n,t) ->
          let p = any_param_multiple ("att_" ^ n) in
	  let opt = any_param ("opt_" ^ n) in
	  let defined =
	    if option_semantics then
	      opt <> ""
	    else
	      p <> []
	  in
	  List.iter (set_back_input ("att_" ^ n)) p;
	  set_back_input ("opt_" ^ n) opt)
	  (physical_typedef_of_scheme scheme);
      (* now fetch records. This may fail because of invalid input *)
      let ex = ref [] in
      current_record <-
	List.flatten
	  (List.map
	     (fun (n,t) -> 
	       try [n, List.assoc n current_record] with Not_found -> [])
	     (derived_typedef_of_scheme scheme))
	@
	List.map
	  (fun (n,t) ->
            let p = String.concat ":" (any_param_multiple ("att_" ^ n)) in
	    let opt = any_param ("opt_" ^ n) in
	    let defined =
	      if option_semantics then
		opt <> ""
	      else
		p <> ""
	    in
            n, 
	    try
	      if defined then 
		mk_field t p 
	      else (
		self # check_null t;
		null
	      )
	    with
	      User_error x ->
		ex := (n,x) :: !ex;
		set_back_input ("tag_" ^ n) (self # error_color);
		Field.null)
	  (physical_typedef_of_scheme scheme);
      if !ex <> [] then
	raise(User_error("While scanning your inputs the following errors have been detected:<br>" ^ 
			 String.concat 
			   "<br>"
			   (List.map
			      (fun (n,x) ->
				"Field " ^ escape_html n ^ ": " ^ x)
			      !ex)))
	  

    method check_null t =
      match t with
	T_constrained (t',checker) ->
	  checker null;
	  self # check_null t'
      |	_ ->
	  ()

    method fetch_back_inputs() =
      (* override values in 'current_record' by applying BACK_INPUTS *)
      (* also overrides 'tags' *)
      let bi = back_inputs() in
      let ptd = physical_typedef_of_scheme scheme in
      let option_semantics = any_param "option_semantics" <> "" in
      if bi <> [] then
	current_record <-
	  List.map
	    (fun (n,v) ->
	      try
 	        let t = List.assoc n ptd in      (* may raise Not_found *)
		let p = 
		  String.concat
		    ":"
		    (List.flatten (List.map (fun (n',v') ->
		                               if n' = "att_" ^ n then
						 [v']
					       else
						 [])
				            bi)) in
		let opt = try List.assoc ("opt_" ^ n) bi 
		          with Not_found -> "" in
		let defined =
		  if option_semantics then
		    opt <> ""
		  else
		    p <> ""
		in
		n, 
		(if defined then 
		   mk_unchecked_field t p
		else null)
	      with
		Not_found -> (n,v))
	    current_record;
      tags <-
	List.map
	  (fun (n,v) ->
	    try
	      n, List.assoc ("tag_" ^ n) bi
	    with
	      Not_found ->
		n, self # default_color)
	  current_record


    method load_record id =
      (* load 'current_record' via "id" parameter *)
      let r = table # get_record id in
      current_record <-
	List.map
	  (fun (n,t) -> n, r # field n)
	  (typedef_of_scheme scheme);
      current_id <- id;
      tags <-
	List.map
	  (fun (n,v) ->
	    n, self # default_color)
	  current_record


    method reply_input_form() =
      (* compute parameters for an "add" or "change" form *)
      let conf = self # default_config current_record in
      let input =
	self # html_input_form_spec (self # use_javascript()) current_record in
      let output =
	List.flatten
	  (List.map
	     (fun (n,t) ->
	       let n' = "att_" ^ n in
	       let n'' = "ro_" ^ n in
	       if List.mem_assoc n' input then
		 []
	       else
		 try
		   let v = List.assoc n current_record in
		   [n'', 
		     Verbatim (html_output [Config (conf, [Field(n,v)])])]
		 with
		   Not_found -> [n'', Escape_html "Unknown"])
	     (typedef_of_scheme scheme))
      in

      input @
      output @
      (List.map
	 (fun (n,v) ->
	   "tag_" ^ n, Verbatim v)
	 tags)


    method reply_output_form() =
      let conf = self # default_config current_record in
      let output =
	List.flatten
	  (List.map
	     (fun (n,t) ->
	       let n' = "att_" ^ n in
		 try
		   let v = List.assoc n current_record in
		   [n', 
		     Verbatim (html_output [Config (conf, [Field(n,v)])])]
		 with
		   Not_found -> [n', Escape_html "Unknown"])
	     (typedef_of_scheme scheme))
      in
      output


    (* DYNAMIC DISPATCH *)

    method preaction page =
      match page with
	"add_form"          -> self # preaction_add_form()
      |	"add"               -> self # preaction_add()
      |	"add_form_again"    -> self # preaction_add_form_again()
      |	"change_form"       -> self # preaction_change_form()
      |	"change"            -> self # preaction_change()
      |	"change_form_again" -> self # preaction_change_form_again()
      |	"delete"            -> self # preaction_delete()
      |	"show"              -> self # preaction_show()
      |	"select"            -> self # preaction_select()
      |	_ -> failwith "Edit: unknown sub page"

    method action page =
      match page with
	"add_form"          -> self # action_add_form()
      |	"add"               -> self # action_add()
      |	"add_form_again"    -> self # action_add_form_again()
      |	"change_form"       -> self # action_change_form()
      |	"change"            -> self # action_change()
      |	"change_form_again" -> self # action_change_form_again()
      |	"delete"            -> self # action_delete()
      |	"show"              -> self # action_show()
      |	"select"            -> self # action_select()
      |	_ -> failwith "Edit: unknown sub page"

    method postaction page =
      match page with
	"add_form"          -> self # postaction_add_form()
      |	"add"               -> self # postaction_add()
      |	"add_form_again"    -> self # postaction_add_form_again()
      |	"change_form"       -> self # postaction_change_form()
      |	"change"            -> self # postaction_change()
      |	"change_form_again" -> self # postaction_change_form_again()
      |	"delete"            -> self # postaction_delete()
      |	"show"              -> self # postaction_show()
      |	"select"            -> self # postaction_select()
      |	_ -> failwith "Edit: unknown sub page"

    method reply page =
      match page with
	"add_form"          -> self # reply_add_form()
      |	"add"               -> self # reply_add()
      |	"add_form_again"    -> self # reply_add_form_again()
      |	"change_form"       -> self # reply_change_form()
      |	"change"            -> self # reply_change()
      |	"change_form_again" -> self # reply_change_form_again()
      |	"delete"            -> self # reply_delete()
      |	"show"              -> self # reply_show()
      |	"select"            -> self # reply_select()
      |	_ -> failwith "Edit: unknown sub page"

    method fragment_file page =
      match page with
	"add_form"          -> self # fragment_file_add_form
      |	"add"               -> self # fragment_file_add
      |	"add_form_again"    -> self # fragment_file_add_form
      |	"change_form"       -> self # fragment_file_change_form
      |	"change"            -> self # fragment_file_change
      |	"change_form_again" -> self # fragment_file_change_form
      |	"delete"            -> self # fragment_file_delete
      |	"show"              -> self # fragment_file_show
      |	"select"            -> self # fragment_file_select
      |	_ -> failwith "Edit: unknown sub page"

    (* DEFAULT CONFIGURATION *)

    method preaction_add_form() = ()
    method preaction_add() = ()
    method preaction_add_form_again() = ()
    method preaction_change_form() = ()
    method preaction_change() = ()
    method preaction_change_form_again() = ()
    method preaction_delete() = ()
    method preaction_show() = ()
    method preaction_select() = ()

    method action_add_form() =
      current_record <-
	List.map 
	  (fun (n,t) -> n, Field.null)
	  (physical_typedef_of_scheme scheme);
      current_id <- (-1);
      self # fetch_back_inputs();

    method action_add() =
      current_record <- [];
      self # fetch_record();

    method action_add_form_again() =
      current_record <- [];
      self # fetch_record();
      self # fetch_back_inputs();

    method action_change_form() =
      self # load_record(get_id());
      self # fetch_back_inputs()

    method action_change() =
      current_record <- [];
      let id = get_id() in
      self # fetch_record();
      current_id <- id;

    method action_change_form_again() =
      self # load_record(get_id());
      self # fetch_record();
      self # fetch_back_inputs()

    method action_delete() =
      let id = get_id() in
      current_id <- id

    method action_show() =
      self # load_record(get_id())

    method action_select() =
      selection_list <- 
	List.map 
	  (fun r -> r # id(), r) 
	  (table # select (fun _ -> true))

    method postaction_add_form() = ()
    method postaction_add() = ()
    method postaction_add_form_again() = ()
    method postaction_change_form() = ()
    method postaction_change() = ()
    method postaction_change_form_again() = ()
    method postaction_delete() = ()
    method postaction_show() = ()
    method postaction_select() = ()

    method update_derived_fields() =
      self # load_record(current_id)          (* brute force solution *)

    method reply_add_form() =
      self # reply_input_form()

    method reply_add_form_again() =
      self # reply_input_form()

    method reply_change_form() =
      self # reply_input_form()

    method reply_change_form_again() =
      self # reply_input_form()

    method reply_add() =
      current_id <- table # add_record current_record;
      self # update_derived_fields();
      []

    method reply_change() =
      table # change_record current_id current_record;
      self # update_derived_fields();
      []

    method reply_delete() =
      table # delete_record current_id;
      []

    method reply_show() = 
      self # reply_output_form()

    method reply_select() =
      if selection_list = [] then
	[ "selector", 
	  Verbatim
	    ("<input type=hidden name=id value=\"-1\">" ^
	     self # msg_select_nothing())
	]
      else
	[ "selector", 
	  Verbatim
	    ("<select name=id>\n" ^
	     String.concat
	       "\n"
	       (List.map
		  (fun (i,r) ->
		    "<option value=\"" ^ string_of_int i ^ "\">" ^
		    escape_html(self # msg_select_id i))
		  selection_list) ^
	     "</select>")
	] 

    method msg_select_nothing() = "Sorry, nothing to select" 
    method msg_select_id i = "Record with ID " ^ string_of_int i

    method fragment_file_add_form =    basename ^ ".add_form.fhtml"
    method fragment_file_add =         basename ^ ".add.fhtml"
    method fragment_file_change_form = basename ^ ".change_form.fhtml"
    method fragment_file_change =      basename ^ ".change.fhtml"
    method fragment_file_delete =      basename ^ ".delete.fhtml"
    method fragment_file_show =        basename ^ ".show.fhtml"
    method fragment_file_select =      basename ^ ".select.fhtml"


    method need_write_lock page =
      match page with
	"add_form"          -> false
      |	"add"               -> true
      |	"add_form_again"    -> false
      |	"change_form"       -> false
      |	"change"            -> true
      |	"change_form_again" -> false
      |	"delete"            -> true
      |	"show"              -> false
      |	"select"            -> false
      |	_ -> failwith "Edit: unknown sub page"
	

    method default_config r =
      []


    method use_javascript() =
      false

    method error_color = "red"

    method default_color = "white"

  end
