(* $Id$
 * ----------------------------------------------------------------------
 * 
 *)


type field;;
  (* The value of a field *)


type selection;;

type field_type =       (* Types of fields *)
    T_line
  | T_password
  | T_textbox
  | T_hyperlink
  | T_int
  | T_select of selection
  | T_date
  | T_constrained of (field_type * (field -> unit))
      (* A type with some constraints on its value. It is based on the 
       * 'field_type' argument, and the function argument is applied to
       * all values and must not raise an exception.
       *)
;;

exception No_content;;
  (* raised by some functions if a 'field' argument is 'null' *)

exception Unchecked_content of string
  (* raised by 'dest_*' functions if applied to an unchecked field *)



(* Create 'selection' *)

val mk_selection : 
    string ->                             (* catalogue name *)
    bool ->                               (* multiple selections allowed? *)
    (unit -> (string * string) list) ->   (* return pairs (intval, extval) *)
    (unit -> string list) ->              (* return default value *)
    selection
;;


val get_base_type : field_type -> field_type
  (* Removes any 'T_constrained' constructors, if any *)


val equal_field_types : field_type -> field_type -> bool
  (* Equality for field types. T_selection: In this case, the given selections
   * must be physically the same. T_constrained: In this case, the given
   * check functions must be physically the same.
   *)


(* Create 'field' values from user input *)

val mk_date : int -> int -> int -> field    (* day month year |-> field *)
val mk_date_from_string : string -> field   (* e.g. "12 Jan 1999" |-> field *)
val mk_today : unit -> field                (* the date "today" *)
val mk_line : string -> field
val mk_password : string -> field           (* clear text password |-> field *)
val mk_textbox : string -> field
val mk_hyperlink : string -> field
val mk_int : int -> field
val mk_int_from_string : string -> field
val mk_select : field_type -> string list -> field 
    (* field_type internal_value_list |-> field *)
val mk_select_default : field_type -> field
    (* create the default value for T_select types *)


(* This is a generic function for creating 'field' values: *)

val mk_field : field_type -> string -> field
    (* field_type user_input |-> field *)
    (* Note: if field_type = T_select, the string is a ':' separated list
     * of selected keys.
     * Note: This function can be used to create constrained values.
     *)


val mk_unchecked_field : field_type -> string -> field
    (* Creates an unchecked field value. It can be transformed to a checked
     * value by 'check_field'. Most functions below cannot handle with unchecked
     * values, but especially the html_* functions can.
     *)

val check_field : field -> field
    (* ensures that the field is checked *)


val null : field
    (* The 'null' is an untyped singulary value that is compatible with any
     * (unconstrained) field_type. (You can forbid 'null' values.)
     *)


(* Destroy 'field' values and get components: 
 * - These functions raise No_content if the 'field' is null.
 *   They raise Unchecked_content if the 'field' is unchecked.
 *)

val dest_line : field -> string
val dest_textbox : field -> string
val dest_hyperlink : field -> string list
val dest_int : field -> int
val dest_select : field -> string list
    (* field |-> internal_value_list *)
val dest_date : field -> (int * int * int)
    (* field |-> day,month,year *)

val eq_password : field -> string -> bool
    (* returns true if the given field (which must have type T_password)
     * contains the given string as password
     * - raises No_content if the 'field' is null.
     *)

val field_matches_type : field -> field_type -> bool
  (* Returns true iff the given field matches the given type *)

(* Date values have special properties: *)

val add_delta : field -> int -> field
    (* add to a date 'field' certain days 
     * - raises No_content if the 'field' is null
     *)
val get_delta : field -> field -> int
    (* determine the difference of two date fields
     * - raises No_content one of the 'field's is null
     *)

(* Make HTML text that outputs 'field' values: 
 * - These functions expect non-null 'field' arguments.
 *   These functions can handle with unchecked values.
 *)

val html_output_line : field -> string
val html_output_textbox : bool -> field -> string
    (* The boolean determines whether newline characters should be 
     * transformed into "<br>" tags
     *)
val html_output_int : field -> string
val html_output_date : bool -> field -> string
    (* The boolean determines whether Jan 1, 1970 should be considered
     * as "unknown date"
     *)
val html_output_hyperlink : (string -> string) -> field -> string
    (* The string argument is placed between <a> and </a> *)
val html_output_select : string -> field -> string
    (* The string argument is the separator if multiple choices are
     * selected
     *)

(* a generic function: *)

type out_config_spec =
    Out_textbox of bool        (* Use line breaks in textboxes? *)
  | Out_date of bool           (* If 1 Jan 1970 is the "unknown date" *)
  | Out_hyperlink of string    (* Text to use for hyperlinks *)
  | Out_hyperlink_self         (* Use the URL as text for hyperlinks *)
  | Out_select of string       (* Text to separate selected items *)
;;

type out_format =
    Html of string                    (* constant html text *)
  | Latin1 of string                  (* constant Latin 1 text *)
  | Field of (string * field)         (* name of the field, the field *)
  | Config of ((string * out_config_spec) list * out_format list)
  | List of (string * out_format list)       
      (* the "string" is the HTML separator of the given list *)
;;

val html_output : out_format list -> string
  (* Converts the "format list" into a string. Uses the default configuration
   * [ "*", Out_textbox true; 
   *   "*", Out_date true; 
   *   "*", Out_hyperlink_self;
   *   "*", Out_select ", " ]
   * You can change the configuration with the Config(spec, subformat) 
   * specifier. Here "spec" is a specification that assigns the named 
   * fields specific configurations. E.g.:
   * Config(["bla", Out_hyperlink "link to bla";
   *         "*", Out_hyperlink "link to somewhere"],
   *        subformat)
   * Here, all hyperlinks named "bla" in subformat are displayed as
   * "link to bla" while all other hyperlinks are printed as 
   * "link to somewhere".
   *)



(* Make HTML form elements to input 'field' values: *)

(* Note: The javascript? argument controls whether Javascript code
 * should be created that checks input immediately. 
 * It is guaranteed that browsers that are not Javascript-capable
 * ignore such code.
 * Not all elements implement this argument.
 *
 * - These functions can handle 'null' fields and unchecked values.
 *)

val html_input_line : string -> int -> field -> string
    (* name width field *)
val html_input_textbox : string -> int -> int -> field -> string
    (* name width height field *)
val html_input_password : string -> int -> field -> string
    (* name width field
     * -- 'field' is only used if it is an unchecked value
     *)
val html_input_int : string -> bool -> int -> field -> string
    (* name javascript? width field *)
val html_input_date : string -> bool -> int -> field -> string
    (* name javascript? width field *)
val html_input_hyperlink : string -> bool -> int -> field -> string
    (* name javascript? width field *)
val html_input_select_asbox 
    : string -> int -> field -> string
	(* name width field *)


(* Sorting and searching *)

val compare_fields : field_type -> field -> field -> int
    (* compares two fields of the given type. 
     * error if one of the fields is not of the passed type
     * - A 'null' value is bigger than any other value (sic!)
     *)

val string_of_field : field_type -> field -> string
    (* return a normalized string representation that can be used for
     * searching (substring searching)
     * - A 'null' value is returned as empty string
     *)


(* Load 'field' values from database or store into it:
 * - These function do not work with 'null' values
 *)

val load_field : field_type -> string -> field
val store_field : field_type -> field -> string


(* Output fields for debugging purposes: *)

val debug_print_field : field -> string
val debug_print_field_type : field_type -> string
