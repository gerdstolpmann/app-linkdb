(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Common

type scheme =                         (* opaque! *)
    { types      : (string * Field.field_type) list;
      unique     : string list;
      derived    : string list;
     (* catalogues : (string * (string * string)) list *)
    }
;;


(* types: declares types for attributes. Note that "_ID_" must not be declared
 *        here.
 * unique: lists attribute names whose values must be unique.
 * catalogues: pairs (catalogue_name, (key_att, value_att)).
 *        Catalogues are views on tables that are used to set up selection
 *        lists. A table that should work as catalogue must have two
 *        special attributes key_att and value_att. If the user wants to
 *        input a selection list he is presented all values of the value_att
 *        that are stored in the table, and the corresponding key_att values
 *        are stored in the database.
 * derived: Attributes listed here are not stored physically. Instead, if
 *        the value of such an attribute must be accessed it is computed by
 *        the given function.
 *)



let mk_scheme attributes unique derived = 
  let rec dups l =
    match l with
      n :: l' ->
	if List.mem n l' then true else dups l'
    | [] ->
	false
  in

  (* (1): check: no duplicate names in 'attributes' *)
  if dups (List.map fst attributes) then
    failwith "mk_scheme: duplicate attributes";

  (* (2): check: no "_ID_" in 'attributes' *)
  if List.mem_assoc "_ID_" attributes then
    failwith "mk_scheme: _ID_ must not be declared here";

  (* (3): check: no duplicate names in 'unique' *)
  if dups unique then
    failwith "mk_scheme: duplicate names in 'unique'";

  (* (4): all members of 'unique' must be declared in 'attributes' *)
  if List.exists (fun n -> not (List.mem_assoc n attributes)) unique then
    failwith "mk_scheme: unknown attribute is declared as 'unique'";

  (* (5): check: no duplicate names in 'derived' *)
  if dups derived then
    failwith "mk_scheme: duplicate derived attribute";

  (* (6): check: all members of 'derived' must be declared in 'attributes' *)
  if List.exists (fun n -> not (List.mem_assoc n attributes)) derived then
    failwith "mk_scheme: unknown attribute is declared as 'derived'";

  { types = attributes;
    unique = unique;
    derived = derived;
    (* catalogues = [] *)
  } 
;;


let typedef_of_scheme sch =
  sch.types
;;

let derived_attributes sch =
  sch.derived
;;

let unique_attributes sch =
  sch.unique
;;


let physical_typedef_of_scheme s =
  (* return only the physically stored attributes from s.types *)
  let rec pt l =
    match l with
      (n,v)::l' ->
	if List.mem n (derived_attributes s) then
	  pt l'
	else
	  (n,v)::(pt l')
    | [] -> []
  in
  pt (typedef_of_scheme s)
;;


let derived_typedef_of_scheme s =
  (* return only the derived attributes from s.types *)
  let rec pt l =
    match l with
      (n,v)::l' ->
	if List.mem n (derived_attributes s) then
	  (n,v)::(pt l')
	else
	  pt l'
    | [] -> []
  in
  pt (typedef_of_scheme s)
;;


let is_sub_scheme scheme' scheme =
  (* tests if scheme' is a sub scheme of scheme *)

  try
    Logging.write Logging.Debug "testing types...";

    List.iter
      (fun (n,t) ->
	Logging.write Logging.Debug ("testing " ^ n);
	let t' = List.assoc n scheme.types in     (* may raise Not_found *)
	if not (Field.equal_field_types t t') then
	  raise Not_found)
      scheme'.types;

    Logging.write Logging.Debug "testing uniqueness...";
    List.iter
      (fun n -> if not (List.mem n scheme.unique) then raise Not_found)
      scheme'.unique;

    List.iter
      (fun n -> if List.mem_assoc n scheme'.types &
	           not (List.mem n scheme'.unique) then raise Not_found)
      scheme.unique;

    Logging.write Logging.Debug "testing derivedness...";
    List.iter
      (fun n -> if List.mem_assoc n scheme'.types &
	        not (List.mem n scheme'.derived) then raise Not_found)
      scheme.derived;

    true
  with
    Not_found -> false
;;


let intersection schemes =
  if schemes = [] then
    failwith "Scheme.intersection: empty list";
  let rec set_isect sets =  (* intersection of sets : 'a list list *)
    match sets with
      [] -> []
    | set1 :: set' ->
	let set2 = set_isect set' in
	List.fold_left
	  (fun s x -> if List.mem x set2 then x::s else s)
	  []
	  set1
  in
  let type_names = 
    set_isect
      (List.map (fun sch -> List.map fst sch.types) schemes) in
  let scheme0 = List.hd schemes in
  let scheme' =
    mk_scheme 
      (List.map (fun n -> n, List.assoc n scheme0.types) type_names)
      (set_isect [type_names; scheme0.unique])
      type_names
  in
  (* check compatibility *)
  List.iter
    (fun scheme ->
      if not (is_sub_scheme scheme' scheme) then
	failwith "Scheme.intersection: incompatible schemes")
    schemes;
    
  scheme'
;;


let check_type sch fields =
  (* checks whether 'fields' matches the table definition *)

  (* (1) check whether all necessary fields are set *)
  let pt = physical_typedef_of_scheme sch in
  List.iter
    (fun (n,t) ->
      let v =
	try
	  List.assoc n fields 
	with
	  Not_found -> 
		raise (User_error ("Attribute '" ^ n ^ "' is missing"))
      in
      if not (Field.field_matches_type v t) then begin
	Logging.write Logging.Err ("v=" ^ Field.debug_print_field v);
	Logging.write Logging.Err ("t=" ^ Field.debug_print_field_type t);
	failwith ("attribute '" ^ n ^ "' has bad type")
      end)
    pt;
  (* (2) check that no more fields are set *)
  List.iter
    (fun (n,v) ->
      if not (List.mem_assoc n pt) then
	failwith ("unknown attribute: " ^ n))
    fields
