(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* Locking:
 *
 * Semantics: The whole database is locked; there may be any number of
 * db read locks, or exactly one db write lock. If a process wants to
 * obtain a db write lock, further db read locks are refused such that
 * the number of locks can only decrease, and the db can be write-locked
 * if the number of db read locks is zero.
 *)

type lid  (* Lock ID *)

val no_lock : lid


val timeout : int ref
  (* Timeout for the next get_read_lock in seconds. A User_error is raised
   * if a timeout happens. Defaults to 10 seconds.
   * (Timeouts for write locks are handled differently.)
   *)

val current : unit -> lid
  (* get ID of current effective read or write lock. This can be used to
   * check if cached data is still up to date: At the moment t1 the cache
   * is filled. If at t2 the lock ID is still the same, the process has had
   * a lock since t1.
   * Returns no_lock if no lock exists.
   *)

val get_read_lock : unit -> unit
  (* Get a read lock. You may even get several read locks. This functions
   * fails if there is a write lock, even if it is owned by this process
   *)

val get_read_permission : unit -> bool
  (* If this process owns a write lock, returns 'false'.
   * Otherwise, 'get_read_lock' is executed and 'true' is returned.
   *)

val with_read_permission : ('a -> 'b) -> 'a -> 'b
  (* Runs the given function with the given argument and returns the
   * result; the function is run with read permissions.
   *)

val require_read_permission : unit -> unit
  (* NOP if we have a read lock, otherwise failure *)

val release_read_lock : unit -> unit
  (* Release a read lock. You must call 'release_read_lock' as often as
   * you have called 'get_read_lock' before to release all read locks.
   * On process termination, read locks are automatically released.
   *)

val get_write_lock : unit -> unit
  (* Wait for the write lock. 
   * Note: This process may have read locks AND up to one write lock. But
   * any other process must not have read or write locks at the same time.
   *)

val get_write_permission : unit -> bool
  (* If this process owns a write lock, returns 'false'.
   * Otherwise, 'get_write_lock' is executed and 'true' is returned.
   *)

val with_write_permission : ('a -> 'b) -> 'a -> 'b
  (* Runs the given function with the given argument and returns the
   * result; the function is run with write permissions.
   *)

val require_write_permission : unit -> unit
  (* NOP if we have a write lock, otherwise failure *)

val release_write_lock : unit -> unit
  (* Release the write lock.
   * On process termination, the write lock is released automatically.
   *)
