(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* Locking:
 *
 * Semantics: The whole database is locked; there may be any number of
 * db read locks, or exactly one db write lock. If a process wants to
 * obtain a db write lock, further db read locks are refused such that
 * the number of locks can only decrease, and the db can be write-locked
 * if the number of db read locks is zero.
 *
 * Implementation: There is the requirement that if a process crashes
 * (e.g. an "uncaught exception"), all locks held by the process are 
 * released. Because of this, locks are implemented using advisory 
 * file locks; the operating system releases locks in this case 
 * automatically.
 *
 * The "lock" file consists of two bytes, on which two advisory locks
 * are defined:
 * Byte 0: If there is a write lock on this byte, this prevents the process
 *         from getting a db read or write lock.
 *         Otherwise, Byte 1 can be used to get a lock.
 * Byte 1: Any read lock on this byte counts as db read lock. Any write lock
 *         on this byte counts as db write lock.
 *
 * Note: Standard Unix semantics allows it to lock regions of a file that
 * do not exist. So don't worry about that the "lock" file always has zero size.
 *)

open Common;;
open Unix;;

type lid = int;;

let timeout = ref 10;;
  (* in seconds *)

let own_read_locks = ref 0;;
let own_write_lock = ref false;;

let current_id = ref 0;;

let lock_file = ref stdin;;
let lock_file_open = ref false;;
let lock_file_name = "lock";;

let no_lock = 0;;

let reinit() =
  if !lock_file_open then (
    Unix.close !lock_file   (* implicitly releases all locks! *)
  );
  own_read_locks := 0;
  own_write_lock := false;
  current_id := 0;
  lock_file := stdin;
  lock_file_open := false
;;


Cgi.register_init reinit ;;


let current() =
  if !own_read_locks > 0 || !own_write_lock then
    !current_id
  else
    no_lock
;;


let get_read_lock() =
  Logging.write Logging.Debug "get_read_lock";
  if !own_read_locks > 0 || !own_write_lock then
    incr own_read_locks
  else
    begin

      incr current_id;

      if not !lock_file_open then
	lock_file := openfile (!basedir ^ "/" ^ lock_file_name) [O_RDWR; O_CREAT] 0o644;
      lock_file_open := true;

      let get_lock() =
	begin try
	  let _ = lseek !lock_file 0 SEEK_SET in
	  lockf !lock_file F_TEST 1;
	  let _ = lseek !lock_file 1 SEEK_SET in
	  lockf !lock_file F_TRLOCK 1;
	  true
	with
	  Unix_error(EACCES, _, _) -> false
	| Unix_error(EAGAIN, _, _) -> false
	end

      in
      
      let c = ref (-1) in
      while not(get_lock()) do
	incr c;
	if !c > !timeout then
	  raise(User_error "Cannot get read lock for the database; try again later");
	sleep 1
      done;

      incr own_read_locks
    end
;;


let get_read_permission() =
  if !own_write_lock then
    false
  else begin
    get_read_lock();
    true
  end
;;
    

let release_read_lock() =
  if !own_read_locks = 0 then
    failwith "Lock.release_read_lock: no read locks exist";

  Logging.write Logging.Debug "release_read_lock";

  decr own_read_locks;

  if !own_read_locks = 0 & not !own_write_lock then begin
    let _ = lseek !lock_file 1 SEEK_SET in
    lockf !lock_file F_ULOCK 1;
  end
;;


let with_read_permission f x =
  let locked = get_read_permission() in
  try
    let y = f x in
    if locked then
      release_read_lock();
    y
  with
    any ->
      if locked then
	release_read_lock();
      raise any
;;


let require_read_permission() =
  if !own_read_locks = 0 & not !own_write_lock then
    failwith "require_read_permission"
;;


let get_write_lock() = 
  if !own_write_lock then
    failwith "Lock.get_write_lock: deadlock condition recognized";

  Logging.write Logging.Debug "get_write_lock";

  if !own_read_locks = 0 then
    incr current_id;

  if not !lock_file_open then
    lock_file := openfile (!basedir ^ "/" ^ lock_file_name) [O_RDWR; O_CREAT] 0o644;
  lock_file_open := true;

  let get_inhibitor() =
    begin try
      let _ = lseek !lock_file 0 SEEK_SET in
      lockf !lock_file F_TLOCK 1;
      true
    with
      Unix_error(EACCES, _, _) -> false
    | Unix_error(EAGAIN, _, _) -> false
    end
  in

  let get_lock() =
    begin try
      let _ = lseek !lock_file 1 SEEK_SET in
      lockf !lock_file F_TLOCK 1;
      true
    with
      Unix_error(EACCES, _, _) -> false
    | Unix_error(EAGAIN, _, _) -> false
    end
  in
      
  let c = ref (-1) in
  (* If we do not get the "inhibitor" lock immediately, give up. This avoids
   * deadlocks.
   *)
  if not(get_inhibitor()) then
    raise(User_error "Cannot get write lock for the database; try again later");

  if !own_read_locks > 0 then begin
    let _ = lseek !lock_file 1 SEEK_SET in
    lockf !lock_file F_ULOCK 1;
  end;

  while not(get_lock()) do
    incr c;
    if !c > !timeout + 5 then begin
      let _ = lseek !lock_file 1 SEEK_SET in
      lockf !lock_file F_ULOCK 1;
      if !own_read_locks > 0 then begin
	let _ = lseek !lock_file 1 SEEK_SET in
	lockf !lock_file F_TRLOCK 1
      end;
      raise(User_error "Cannot get write lock for the database; try again later");
    end;
    sleep 1
  done;

  own_write_lock := true
;;
	

let get_write_permission() =
  if !own_write_lock then
    false
  else begin
    get_write_lock();
    true
  end
;;
    

let release_write_lock() =
  if not !own_write_lock then
    failwith "Lock.release_write_lock: no write lock";

  Logging.write Logging.Debug "release_write_lock";

  let _ = lseek !lock_file 1 SEEK_SET in
  lockf !lock_file F_ULOCK 1;

  if !own_read_locks > 0 then begin
    let _ = lseek !lock_file 1 SEEK_SET in
    lockf !lock_file F_TRLOCK 1
  end;

  let _ = lseek !lock_file 0 SEEK_SET in
  lockf !lock_file F_ULOCK 1;

  own_write_lock := false
;;


let with_write_permission f x =
  let locked = get_write_permission() in
  try
    let y = f x in
    if locked then
      release_write_lock();
    y
  with
    any ->
      if locked then
	release_write_lock();
      raise any
;;


let require_write_permission() =
  if not !own_write_lock then
    failwith "require_write_permission"
;;
