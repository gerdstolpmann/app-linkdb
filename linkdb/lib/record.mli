(* $Id$
 * ----------------------------------------------------------------------
 *
 *)


class type record_type =
  object
    method id : unit -> int
    method field : string -> Field.field
    method setrec : (string * Field.field) list -> unit
  end

class type table_type =
  object
    method get_derived_field : record_type -> string -> Field.field
  end

class record :
    #table_type ->
    Scheme.scheme -> int -> (string * Field.field) list ->
      object
	method id : unit -> int
	method field : string -> Field.field
	method setrec : (string * Field.field) list -> unit
      end

val field : record -> string -> Field.field
val id : record -> int
