(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* This module manages the known tables and catalogues centrally. *)


val register_table : string -> Table.t -> unit
  (* register_table name table: registers 'table' under 'name' *)

val register_catalogue_directly :
    string -> (string * (string * bool)) list -> bool -> unit
  (* register_catalogue_directly cat_name cat is_multiple:
   * registers catalogue 'cat_name' which is the list 'cat' of
   * (key, (value, is_default)) triples. 'is_multiple' indicates
   * whether multiple selections are allowed in this catalogue.
   *)

val register_catalogue_exd : 
    string -> string -> string -> string -> bool -> string list -> unit
  (* register_catalogue_exd cat_name table_name key_att_name value_att_name
   *   is_multiple default:
   * registers catalogue 'cat_name' which is derived from 'table_name'.
   * The attribute 'key_att_name' is used for the keys, the attribute
   * 'value_att_name' is used for the values. 'is_multiple' indicates
   * whether multiple selections are allowed in this catalogue. 'default'
   * is the list of keys for the default value.
   *
   * Note: It is sufficient if the table 'table_name' is registered at the
   *   time of the first access of the catalogue
   * Note: "exd" = explicit default
   *)

val register_catalogue_imd : 
    string -> string -> string -> string -> bool -> string -> unit
  (* register_catalogue_imd cat_name table_name key_att_name value_att_name
   *   is_multiple default_name:
   * registers catalogue 'cat_name' which is derived from 'table_name'.
   * The attribute 'key_att_name' is used for the keys, the attribute
   * 'value_att_name' is used for the values. 'is_multiple' indicates
   * whether multiple selections are allowed in this catalogue. 'default'
   * is the attribute that is non-null for all default selections.
   *
   * Note: It is sufficient if the table 'table_name' is registered at the
   *   time of the first access of the catalogue
   * Note: "imd" = implicit default
   *)
  
val get_table : string -> Table.t
  (* get a previously registered table or raise Not_found. On the first
   * call of 'get_table' with a certain table name the table is loaded
   * from the external store.
   * Note: You need a read lock!
   *)

val get_scheme : string -> Scheme.scheme
  (* get the scheme of a previously registered table or raise Not_found *)

val get_selection : string -> Field.selection
  (* get_selection cat_name:
   * returns the Field.selection which is created from the catalogue
   * 'cat_name'
   *)

val get_record : int -> Record.record
  (* Finds the records with the given ID in any table, or raises Not_found *)

val begin_transaction : unit -> unit
  (* Begins a new transaction. This function initializes
   * the state of the transaction.
   * It is required that a write lock exists.
   * Note: You must be outside of a transaction to begin a new transaction,
   * i.e. nested transactions are not supported.
   *)

val commit_transaction : unit -> unit
  (* Writes all modified tables.
   * After this, the transaction has been finished.
   *)

val abort_transaction : unit -> unit
  (* Ends the transaction without writing the modified tables.
   * After this, the transaction has been finished.
   *)
