(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* TODO:
 * - <select> requires that (n1=v1), (n2=v2) parameters with n1=n2 may
 *   be sent to the server
 *)

open Wbuffer;;
open Str;;

let webmaster = "mailto:gerd@gerd-stolpmann.de";;

let basedir = Config.db_dir
  (* The directory that stores the *.db files *)

let navigator = "navigator.fragment";;

  (* navigator: a piece of HTML that is inserted as navigation bar
   * when the frameless version is selected.
   *)

exception User_error of string;;

  (* a User_error is reported in a different way. The assumption is that
   * the user has entered something bad or wants to do an action that
   * cannot be done. The HTML page generated on a User_error is rather
   * polite ("Sorry, I can't do that") and gives a hint what went wrong.
   * -- In contrast to this, all other exceptions are treated as 
   * software errors for which the author of the software is responsible.
   *)

let user_error s = raise (User_error s)
;;



(********* Some functions that are useful anywhere ***********)

let subset s t =
  (* returns true iff s is a subset of t *)
  List.for_all (fun x -> List.mem x t) s
;;


let trim s =
  (* removes spaces from the beginning and end of a string *)
  let l = String.length s in
  let k1 = ref 0 in
  while !k1 < l & s.[!k1] = ' ' do
    incr k1
  done;
  let k2 = ref (l-1) in
  while !k2 >= 0 & s.[!k2] = ' ' do
    decr k2
  done;
  if !k1 <= !k2 then
    String.sub s !k1 (!k2 - !k1 + 1)
  else
    ""
;;


let form_http_re = Str.regexp "^http://";;
let form_ftp_re = Str.regexp "^ftp://";;
let form_mailto_re = Str.regexp "^mailto:[^@]+@[^@]+$";;

let is_url s =
  (* return true iff 's' is a valid URL.
   * The implementation is not perfect. Currently, the criterion for URLs
   * is as follows:
   * (a) URLs of the form "http://anything" and "ftp://anything" are valid.
   * (b) URLs of the form "mailto:anything@anything" are valid
   * Note that there are much more protocols that we deny here.
   *)
  Str.string_match form_http_re s 0 or
  Str.string_match form_ftp_re s 0 or
  Str.string_match form_mailto_re s 0 
;;


let months = 
  (* DEPRECATED *)
  [| "Jan"; "Feb"; "Mar"; "Apr"; "May"; "Jun"; "Jul"; "Aug"; "Sep";
     "Oct"; "Nov"; "Dec" |]
;;


let date_string d =
  (* DEPRECATED *)
  (* d: the number of DAYS since 1 Jan 1970. Note that this is a different
   * format than standard Unix time values that count the SECONDS since
   * 1 Jan 1970. (We are only interested in a precision of days.)
   * This function returns a date string like "15 Aug 1999".
   * If d=0, "unknown date" is returned.
   *)
  if d = 0 then
    "unknown date"
  else
    let t = Unix.localtime (float d *. 86400.0) in
    let mday = t.Unix.tm_mday in
    let mon = t.Unix.tm_mon in
    let year = t.Unix.tm_year + 1900 in
    string_of_int mday ^ " " ^ months.(mon) ^ " " ^ string_of_int year
;;


let hex_digits = [| '0'; '1'; '2'; '3'; '4'; '5'; '6'; '7';
                    '8'; '9'; 'a'; 'b'; 'c'; 'd'; 'e'; 'f' |];;

let encode_hex s =
  (* encode an arbitrary string as a sequence of hexadecimal digits *)
  let l = String.length s in
  let t = String.make (2*l) ' ' in
  for n = 0 to l - 1 do
    let x = Char.code s.[n] in
    t.[2*n]   <- hex_digits.( x lsr 4 );
    t.[2*n+1] <- hex_digits.( x land 15 );
  done;
  t
;;


let hex_digest s =
  (* hex encoding of MD5 message digest. This function is intended to store
   * passwords in the database such that clear text is avoided. If you
   * store only MD5(password) it is very hard to invert the MD5 application,
   * i.e. to compute the password from MD5(password). For authentication
   * purposes, storing of MD5(password) suffices, as the password comes
   * in clear text from the browser and it can be compared with the stored
   * MD5(password).
   * Note that the security improvement is not very dramatic, though.
   *)
  encode_hex (Digest.substring s 0 (String.length s))
;;


(************* decode CGI parameters ******************)


let password_re = regexp "password";;


let get_params() =
  let p = Cgi.parse_args() in
  List.iter
    (fun (name,value) ->
       if name <> "AUTO" then begin
	 if string_match password_re name 0 then
	   Logging.write Logging.Param (name ^ "= ***") 
             (* do not log parameters that have "password" as substring in
                * their name
              *)
	 else
	   Logging.write Logging.Param (name ^ "=" ^ value)
       end
    )
    p;
  p

let params = 
  (* params: is the list of (name,value) pairs of received CGI parameters.
   * Lazy evaluation forces that decoding of parameters is done when the
   * first access comes. Note that the parameters are logged in this case.
   *
   * Note: 'params' are read-only. They always reflect the parameters passed
   * on invocation.
   *)
  ref(lazy(get_params()))
;;


Cgi.register_init (fun () -> params := lazy(get_params()));;


let param name =
  (* get CGI parameter 'name' with whitespace removed at beginning and end.
   * This removal is done because we assume that normal CGI parameters result
   * from a user dialogue.
   * For other purposes, "automatic parameters" (see below) should be used.
   *)
  (* non-existing parameters are returned as "" *)
  let p = Lazy.force !params in
  let v = try List.assoc name p with Not_found -> "" in
  trim v
;;


let param_multiple name =
  (* get all definitions of the CGI parameter 'name' as list. Whitespace is
   * trimmed as in 'param' above.
   *)
  let p = Lazy.force !params in
  List.flatten (List.map (fun (n,v) -> if n = name then [trim v] else []) p)
;;


let param_exists name =
  (* does the CGI parameter 'name' exist? *)
  let p = Lazy.force !params in
  List.mem_assoc name p
;;


let param_list () =
  (* return a list of CGI parameter names *)
  List.map 
    (fun (name,value) -> name)
    (Lazy.force !params)
;;

(**************** automatic parameters ************************)

(* Some parameters need to be passed through several pages. It would be 
 * inconvenient if each of these parameters would be passed separately,
 * because this would have to be done in all pages again.
 * Here we introduce the CGI parameter "AUTO" that is an extensible
 * container for other parameters. This means that one "official" CGI
 * parameter transports several other parameters. HTML pages should
 * be designed such that they always pass "AUTO" to all other pages to which
 * hyperlinks exist.
 *
 * EXAMPLE:
 *
 * Consider a series of HTML pages A, B, and C, where A and B are mainly
 * HTML forms. Normally, submission of the form in A invokes a CGI program
 * A.cgi that outputs B, and submission of the form in B invokes another
 * program B.cgi that outputs finally C. It is very common that the user
 * must enter some fields in A whose values must still be available in B.cgi
 * and probably appear in B and C as plain text. 
 *
 * A  --SUBMIT-->  A.cgi  --OUTPUTS-->  B  --SUBMIT-->  B.cgi  --OUTPUTS-->  C
 *
 * To solve this, HTML forms may have hidden fields that are not displayed
 * but whose values are simply carried through. In traditional CGI solutions,
 * every value in A that must be available in B, B.cgi, or C is transported
 * separately in hidden fields. If the application grows, often further 
 * parameters must be added that should be handled in this way. Automatic
 * parameters simplify this, as several values that should persist after
 * A.cgi has finished can be packed into one value, "AUTO", and B needs
 * only to pass "AUTO" as hidden field to B.cgi. If you add more parameters,
 * these are simply marked as being automatic, and they are thus packed into
 * "AUTO", too, and passed to all subsequent forms and programs by default.
 *
 * Note that automatic parameters are very interestant when A=C, i.e. the
 * sequence is in fact a cycle. "AUTO" may then be regarded as substitute
 * of global variables (analogy: in functional programming languages, global
 * variables can be simulated by passing values, if needed recursively, 
 * through all functions. Here, CGI programs are like functions, and "AUTO"
 * is a collection of variables that is passed through all functions by
 * default.)
 *)

let autoparm_encode s =
  (* use a "hex" like encoding with "digits" "a" to "p" *)
  let l = String.length s in
  let t = String.make (2*l) ' ' in
  for i = 0 to l - 1 do
    let n = Char.code (s.[i]) in
    t.[ 2*i ]     <- Char.chr( 97 + (n lsr 4) );
    t.[ 2*i + 1 ] <- Char.chr( 97 + (n land 15) );
  done;
  t
;;


let autoparm_decode s =
  (* for all s: autoparm_decode (autoparm_encode s) = s *)
  let l = String.length s in
  if l mod 2 <> 0 then
    failwith "autoparm_decode";
  let t = String.make (l/2) ' ' in
  for i = 0 to l/2 - 1 do
    let n1 = Char.code (s.[ 2*i ]) - 97 in
    let n2 = Char.code (s.[ 2*i + 1 ]) - 97 in
    if n1 < 0 or n2 < 0 or n1 > 15 or n2 > 15 then
      failwith "autoparm_decode";
    t.[ i ] <- Char.chr( (n1 lsl 4) lor n2 );
  done;
  t
;;


type auto_t = (string * string) list;;

  (* The "AUTO" container is simply an associative list, with the parameter
   * name as key and an arbitrary string as value. 
   * As with normal parameters, it is allowed to have multiple parameters
   * with the same key.
   *)


let get_auto () =
  let auto_parm = param "AUTO" in
  if auto_parm = "" then 
    []
  else begin
    let v = (Marshal.from_string (autoparm_decode auto_parm) 0 : auto_t) in
    List.iter
      (fun (name,value) -> 
	 if string_match password_re name 0 then
	   Logging.write Logging.Param ("AUTO " ^ name ^ "= ***")
	 else
	   Logging.write Logging.Param ("AUTO " ^ name ^ "=" ^ value))
      v;
    v
  end


let auto = 
  (* The "AUTO" container is decoded when first accessed. Note that "auto"
   * is a ref variable, and thus it is possible to add parameters to "auto".
   *)
  ref (lazy(get_auto()))
;;


Cgi.register_init (fun () -> auto := lazy(get_auto())) ;;


let auto_param name =
  (* get the string value of the automatic parameter "name".
   * Unlike "param" whitespace is not removed (automatic parameters are 
   * normally not used for HTML forms).
   * "" is returned if the parameter does not exist.
   *)
  try
   List.assoc name (Lazy.force !auto)
  with
    Not_found -> ""
;;


let auto_param_multiple name =
  (* get all definitions of the automatic parameter 'name' as list. *)
  let p = Lazy.force !auto in
  List.flatten (List.map (fun (n,v) -> if n = name then [v] else []) p)
;;


let auto_param_exists name =
  (* Does an automatic parameter with "name" exist? *)
  List.mem_assoc name (Lazy.force !auto)
;;


let set_auto_param name value =
  (* sets the value of the automatic parameter called 'name' to 'value'.
   * 'value' may be an arbitrary string that may contain any non-printable
   * character as well (including '\000')
   * If there are multiple parameters 'name', only one parameter called 'name'
   * will remain.
   *)
  let rec enter l =
    match l with
      [] -> [ name,value ]
    | (n,v) :: l' -> if n=name then (n,value)::(discard l') 
                               else (n,v)::(enter l')
  and discard l =
    match l with
      [] -> []
    | (n,v) :: l' -> if n=name then discard l' 
                               else (n,v)::(discard l')
  in
  let old_list = Lazy.force !auto in
  auto := lazy (enter old_list)
;;


let unset_auto_param name =
  (* removes the parameter with 'name' from the set of automatic parameters.
   * Nothing is done if such a parameter does not exist.
   *)
  let old_list = Lazy.force !auto in
  let new_list = 
    List.flatten
      (List.map (fun (n,v) -> if n = name then [] else [n,v])
	        old_list) in
  auto := lazy new_list
;;


let set_auto_param_multiple name values =
  (* sets a value for 'name' for each member of 'values' *)
  unset_auto_param name;
  let new_list =
    (List.map (fun v -> (name,v)) values) @
    (Lazy.force !auto) in
  auto := lazy new_list
;;


let any_param name =
  (* If existent, returns the value of the "classical" parameter 'name'.
   * Otherwise, returns the value of the automatic parameter 'name'.
   * Returns "" if neither parameter exist.
   *)
  if param_exists name then 
    param name
  else
    auto_param name
;;


let any_param_multiple name =
  (* If existent, returns values of the classical parameter 'name'.
   * Otherwise returns values of the automatic parameter 'name'
   *)
  if param_exists name then
    param_multiple name
  else
    auto_param_multiple name
;;


let encoded_auto_params() =
  (* get the "AUTO" value prepared for insertion into a "hidden" form
   * element.
   * I.e. if you have a form
   * <FORM NAME=... ACTION=... METHOD=POST>
   * ...
   * <INPUT TYPE=HIDDEN NAME="AUTO" VALUE="XXX">
   * ...
   * </FORM>
   * this function computes what should be inserted as XXX. Note that only
   * letters are used to represent the AUTO value.
   * Another note: You are strongly encouraged to use POST as transport
   * method, because the alternative GET has limitations on the maximum
   * length of parameters.
   *)
  autoparm_encode (Marshal.to_string 
		     (Lazy.force !auto) 
		     [ Marshal.No_sharing ])
;;


(**************** deal with special encodings *****************)

let htmltab =
  (* An array of length 256, that maps character codes to strings that may
   * be used in HTML text to represent the characters.
   * Note: Only characters 32 to 127 and 160 to 255 are represented.
   *)
  let a = Array.create 256 "" in
  for k = 32 to 127 do
    a.(k) <- String.make 1 (Char.chr k)
  done;
  a.(9)  <- "\t";   (* may be sensible in preformatted text *)
  a.(10) <- "\n";
  a.(60) <- "&lt;";
  a.(62) <- "&gt;";
  a.(38) <- "&amp;";
  a.(34) <- "&quot;";
  (* The following is possible because OCAML as well as HTML use the Latin 1
   * alphabet as default if no alphabet is specified.
   * Of course, it would be better to represent these characters by their
   * names and not numbers, as this works in misconfigured browsers better.
   *)
  for k = 160 to 255 do
    a.(k) <- "&#" ^ string_of_int k ^ ";"
  done;
  a
;;


let escape_html s =
  (* Transforms:
   * - '<' to "&lt;",
   * - '>' to "&gt;",
   * - '&' to "&amp;",
   * - '"' to "&quot;",
   * - any character with code from 160 to 255 to "&#<n>;"
   * This function is useful to embed arbitrary text in given HTML pages.
   * For example, you may have a page:
   * <HTML> ... <BODY> ... Your ID is: XXX ... </BODY> ... </HTML>
   * where XXX should be replaced by a user ID that has been previously
   * entered. As the replacement text for XXX may contain characters such
   * as <,>,&," that are used for HTML tags, too, these characters must be
   * transformed to their &symbolic_name; before insertion.
   * OCAML SCANNER PROBLEM: "
   *)
  let t = ref "" in
  for k = 0 to String.length s - 1 do
    let c = s.[ k ] in
    t := !t ^ htmltab.( Char.code c )
  done;
  !t
;;


let escape_quotes s =
  (* Protects double quotes with backslashes. I.e. the string abc"d is 
   * transformed into abc\"d, and abc\d is transformed into abc\\d.
   * This function is useful to generate Javascript strings.
   * OCAML SCANNER PROBLEM: "
   *)
  let t = ref "" in
  for k = 0 to String.length s - 1 do
    let c = s.[ k ] in
    t := !t ^ 
      if c = '"' then "\\\"" else if c = '\\' then "\\\\" else String.make 1 c
  done;
  !t
;;

(**************** BACK to the last page on error ***************)

(* The problem is that once an error happens, the user wants to correct
 * the inputs and try again. The system should offer a way to
 * go exactly to the last page. 
 * Note: Browsers usually have a "Back" button but this button does not work
 * with pages that are the result of POST actions.
 *
 * CGI Parameters:
 * BACK:          The URL of the last page, without any parameters
 * BACK_PARAMS:   The CGI parameters that were given at the moment
 *                the last page was called
 * BACK_INPUTS:   Values of input widgets from the last invocation
 *
 * BACK_PARAMS and BACK_INPUTS are encoded in the same way as AUTO.
 * To avoid lengthy transfers, the BACK feature is not recursive.
 * This means that BACK_PARAMS do not contain BACK, BACK_PARAMS or
 * BACK_INPUTS as values.
 *
 * A page that wants to offer the "go back" feature, should include
 * in the form where users might enter incorrect values the parameters
 * BACK and BACK_PARAMS as hidden values. BACK is the URL of the page
 * itself; BACK_PARAMS are the CGI parameters from the page invocation.
 * Below is a function to compute BACK_PARAMS.
 *
 * On an error page, there should be a form that has BACK as action,
 * "POST" as method, all parameters from BACK_PARAMS as hidden
 * values, and in BACK_INPUTS it should pass the form inputs back
 * that caused the error. As errors are usually detected by Caml exceptions,
 * there is no way to pass these inputs to the error handler. Because
 * of this, there is a global variable that should be used for this
 * purpose.
 *)

let var_back_inputs = ref ([] : (string * string) list);;
  (* a reference to (name,value) pairs *)


Cgi.register_init (fun () -> var_back_inputs := []) ;;


let back_params exceptions =
  (* Include the result of this function as "BACK_PARAMS" in your form.
   * The result consists only of letters.
   * 'exceptions' is a list of (parameter_name, value) pairs to
   * override the given values.
   *)
  let pl =
    (* Note: The value of "AUTO" is that from the program invocation, not
     * the possibly modified value in 'auto'
     *)
    let params_init = Lazy.force !params in
    let pl =
      List.flatten
	(List.map
	   (fun (n,v) ->
	     if List.mem_assoc n exceptions then [] else [n,v])
	   params_init)
	@ exceptions in
    List.flatten
      (List.map
	 (fun (n,v) ->
	   if n = "BACK" or n = "BACK_PARAMS" or n = "BACK_INPUTS" then
	     []
	   else
	     [n,v])
	 pl) in
  autoparm_encode (Marshal.to_string pl [])
;;


let set_back_input name value =
  (* Store that you have seen the user input 'value' for the field 'name' *)
  (* Note: You can invoke this function several times to set multiple
   * values for 'name'
   *)
  var_back_inputs := (name,value) :: !var_back_inputs
;;


let back_button label =
  (* Creates a string which contains an HTML form describing a "back button"
   * with 'label' on it.
   *)

  let back = param "BACK" in
  let back_params = autoparm_decode(param "BACK_PARAMS") in

  if back = "" then
    ""
  else
    let pl = (Marshal.from_string back_params 0 : (string * string) list) in
    String.concat
      "\n"
      ([ "<form action=\"" ^ escape_html back ^ "\" method=post>" ] @
       List.map
	 (fun (n,v) ->
	   "<input type=hidden name=\"" ^ escape_html n ^ 
	   "\" value=\"" ^ escape_html v ^ "\">")
	 pl
       @
       [ "<input type=hidden name=\"BACK_INPUTS\" value=\"" ^
	     autoparm_encode(Marshal.to_string !var_back_inputs []) ^ "\">";
	 "<input type=submit value=\"" ^ escape_html label ^ "\">";
	 "</form>"
       ])
;;


let back_inputs() =
  (* Decodes the "BACK_INPUTS" parameter if present *)
  let p = param "BACK_INPUTS" in
  if p = "" then
    []
  else
    (Marshal.from_string (autoparm_decode p) 0 : (string * string) list)
;;


(******************** CGI BASED SUBPROGRAMS ************************)

(* The idea is to save all 'current' parameters in a special parameter,
 * invoke the subprogram, and return to the caller by restoring the
 * original parameters.
 * Please note that the caller must be a CGI program with a certain 
 * amount of idempotency, i.e. it must be able to be invoked in a 
 * loop.
 *
 * THE CALL:
 *
 * - A form is posted with some special parameters and many ordinary
 *   parameters. This means: Call the subprogram by applying the special
 *   parameters, and after return, invoke the caller again with the
 *   ordinary parameters.
 * - Parameters needed to call
 *
 *   SUB_SCRIPT:             The script to invoke (MUST be a relative path)
 *   SUB_PASS_x:             Pass 'x' to the script with given value
 *   SUB_AUTO_x:             Pass the value of the AUTO parameter 'x'
 *   SUB_RETURN_TO:          Return to this URL
 *   SUB_RETURN_NAME:        Describe return address
 *
 *   all other parameters:   are saved during the call and applied to the
 *                           return script
 * - The script that performs the call does a redirect to SUB_SCRIPT,
 *   and passes all parameters x for which there is a SUB_PASS_x.
 * - All other parameters are saved in SUB_SAVED which is part of AUTO.
 *
 * THE RETURN:
 *
 * - SUB_SAVED is analyzed and a form is constructed upon its contents.
 * - The action of the form is SUB_RETURN_TO.
 * - The hidden parameters are taken from SUB_SAVED, too.
 * - Normally, the activated script is the caller script, and the action
 *   is idempotent (does nothing).
 *)


let rec redirect_call() =
  (* This is a function that performs the redirect *)
  
  let sub_script = param "SUB_SCRIPT" in

  let sub_passed =
    List.flatten
      (List.map 
	 (fun (n,v) ->
	   if String.length n >= 9 & String.sub n 0 9 = "SUB_PASS_" then
	     [ String.sub n 9 (String.length n - 9), v ]
	   else
	     [])
	 (Lazy.force !params)) in

  let sub_auto_passed =
    List.flatten
      (List.map 
	 (fun (n,v) ->
	     if String.length n >= 9 & String.sub n 0 9 = "SUB_AUTO_" then
	       [ let an = String.sub n 9 (String.length n - 9) in
	         an ]
	     else
	       [])
	 (Lazy.force !params)) in

  redirect_call_this 
    sub_script 
    sub_passed 
    sub_auto_passed 
    (param "SUB_RETURN_TO")
    (param "SUB_RETURN_NAME")
    []

and redirect_call_this 
    sub_script
    sub_passed
    sub_auto_passed
    sub_return_to
    sub_return_name
    sub_return_unset       (* unset parameters on return *)
    =

  if not (string_match (regexp "^.*\\.cgi$") sub_script 0) then
    failwith "not a CGI program to execute";
  
  let sub_auto_passed' =
    List.map (fun n -> n, auto_param n) sub_auto_passed in

  let sub_saved =
    [ "SUB_RETURN_TO", sub_return_to;
      "SUB_RETURN_NAME", sub_return_name
    ] @
    List.flatten
      (List.map
	 (fun (n,v) ->
	   if n = "SUB_SCRIPT" or n = "SUB_RETURN_TO" or 
	     n = "SUB_RETURN_NAME" or List.mem n sub_return_unset or
	     (String.length n >= 9 & String.sub n 0 9 = "SUB_PASS_") or
	     (String.length n >= 9 & String.sub n 0 9 = "SUB_AUTO_") then
	     []
	   else
	     [n,v])
	 (Lazy.force !params)) in

  auto := lazy [];

  set_auto_param 
    "SUB_SAVED"   
    (autoparm_encode(Marshal.to_string sub_saved [ Marshal.No_sharing ]));

  List.iter (fun (n,v) -> set_auto_param n v) sub_auto_passed';

  let sub_params = 
    sub_passed @ [ "AUTO", encoded_auto_params() ] in

  raise(Cgi.Internal_redirect(sub_script, sub_params))
;;


let return_form () =
  (* Returns a string "<form ...> <input type=hidden ...> ..." containing
   * a form beginning for a return button. Note that "</form>" is NOT part
   * of this string.
   * Raises Not_found if there is no caller.
   *)

  let saved = auto_param "SUB_SAVED" in
  if saved = "" then raise Not_found;

  let pl = (Marshal.from_string (autoparm_decode saved) 0 : 
	      (string * string) list) in
  
  let return_to = List.assoc "SUB_RETURN_TO" pl in
  
  "<form method=post action=\"" ^ escape_html return_to ^ "\">\n" ^
  String.concat "\n" 
    (List.flatten
       (List.map
	  (fun (n,v) ->
	    if n = "SUB_RETURN_TO" or n = "SUB_RETURN_NAME" then
	      []
	    else
	      [ "<input type=hidden name=\"" ^ escape_html n ^ "\" value=\"" ^
		escape_html v ^ "\">" ])
	  pl))
;;


let caller() =
  (* Returns the name of the caller or raises Not_found *)
  let saved = auto_param "SUB_SAVED" in
  if saved = "" then raise Not_found;

  let pl = (Marshal.from_string (autoparm_decode saved) 0 : 
	      (string * string) list) in
  
  List.assoc "SUB_RETURN_NAME" pl
;;
