(* $Id$
 * ----------------------------------------------------------------------
 *)

(**********************************************************************)
(* CGI emulation                                                      *)
(**********************************************************************)

val parse_args : unit -> (string * string) list
    (* Decodes the arguments of the CGI and returns them as an association list
     * Works whatever the method is (GET or POST) *)

val header : string -> unit
    (* Prints the content-type header. 
     * the argument is the MIME type (default value is "text/html" if the
     * argument is the empty string) *)

val this_url : unit -> string
    (* Returns the full address of the CGI *)

val this_script : unit -> string
    (* Returns only the script name, without slashes *)

val print : string -> unit
    (* Outputs dynamic content *)

val printf : ('a, unit, string, unit) format4 -> 'a


(* The following code may be useful but is already called in [parse_args] *)

val decode : string -> string
val encode : string -> string
    (* Coding and uncoding of CGI arguments.
     * Code from wserver, (c) Daniel de Rauglaudre, INRIA. *)


(**********************************************************************)
(* CGI registration                                                   *)
(**********************************************************************)

exception Internal_redirect of string * (string * string) list
  (* [Internal_redirect(new_script, new_args)] *)


val register_cgi : name:string -> (unit -> unit) -> unit
  (* [register_cgi ~name f]: Makes a "CGI script" accessible under the 
   * URL path [name], and calls [f] to execute it. While being executed,
   * the functions listed under "CGI emulation" are available.
   *)

val register_static : name:string -> content_type:string -> data:string -> unit
  (* [register_static ~name ~content_type ~data]: Registers static content
   *)

val register_init : (unit -> unit) -> unit
  (* This function is called before a CGI is executed *)

val factory : unit -> Netplex_types.processor_factory
  (* Makes the registered CGIs and static content available as Netplex
   * processor using Nethttpd_plex.nethttpd_factory. Defines a handler
   * "linkdb".
   *)
