(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

type scheme

val mk_scheme : 
    (string * Field.field_type) list ->
    string list ->
    string list ->
    scheme

(* mk_scheme types unique derived:
 * types: declares types for attributes. Note that "_ID_" must not be declared
 *        here.
 * unique: lists attribute names whose values must be unique.
 * derived: Attributes listed here are not stored physically. Instead, if
 *        the value of such an attribute must be accessed it is computed by
 *        the given function.
 *)


val typedef_of_scheme : scheme -> (string * Field.field_type) list
(* Get the type definition of a scheme *)

val derived_attributes : scheme -> string list
(* Get the list of derived attribute *)

val unique_attributes : scheme -> string list
(* Get the list of attributes with unique values *)

val physical_typedef_of_scheme : scheme -> (string * Field.field_type) list
(* Get the type definition of the physical part of a scheme, i.e.
 * get only attributes that are NOT derived.
 *)

val derived_typedef_of_scheme : scheme -> (string * Field.field_type) list
(* Get the type definition of the derived part of a scheme *)

val intersection : scheme list -> scheme
(* forms the intersection of several schems. This requires that the schemes
 * are intersectable:
 * - The attributes of the intersection are the attributes which exist in
 *   all schemes
 * - The attribute types of the intersection must be equal in all schemes
 * - If an attribute is unique in one of the schemes it must be unique
 *   in all schemes
 * Note that all attributes of the intersection are derived.
 *
 * If the schemes are not intersectable, a failure is raised.
 *)

val check_type : scheme -> (string * Field.field) list -> unit
(* checks whether the given value list matches the scheme.
 * Raises error if not matching.
 *)
