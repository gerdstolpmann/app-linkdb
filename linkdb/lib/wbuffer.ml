(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* This module provides a central buffer where CGI program can print their
 * output to.
 * The idea of this buffer is that it collects the output of an application
 * as far as it is present, and that the whole output can be either "committed"
 * (send to stdout) or "rolled back" (deleted) at once. (The terms "commit"
 * and "roll back" come from database transaction systems.)
 * It is necessary to be able to roll back the buffer because the so-far
 * collected output should be replaced by an error message in the case of
 * a sudden exception.
 *)

let increment = 1000;;
let buffer = ref (Array.create increment "");;
let position = ref 0;;

let print s =              (* print into buffer *)
  if !position >= Array.length !buffer then
    buffer := Array.append !buffer (Array.create increment "");
  ( !buffer ).( !position ) <- s;
  incr position
;;

let cgi_commit () =        (* copy buffer to stdout *)
  for k = 0 to !position - 1 do
    Cgi.print (!buffer).(k)
  done;
  position := 0
;;

let cgi_rollback () =      (* discard all in the buffer *)
  position := 0
;;
