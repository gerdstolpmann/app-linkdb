(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* Tables which are completely loaded into memory. This class explains
 * how to select from such tables. It does not specify how the tables
 * are loaded or stored.
 *)

open Record;;
open Scheme;;

exception Found of record;;
exception Result of bool;;



class virtual t the_scheme =
  object (self)
    inherit Table.t

    val dbscheme = the_scheme

    val mutable contents = ( [| |] : record array )
	(* Current contents of the table *)

    method scheme = dbscheme

    method get_record id =
      (* return the record that has the ID 'id'.
       * Raises Not_found if record 'id' does not exist
       *)
      try
	Array.iter
	  (fun r ->
	    if r#id() = id then
	      raise (Found r)) 
	  contents;
	raise Not_found
      with
	Found f -> f


    method get_sorted_records names =
    (* names: a list of attribute names. The records are first sorted by
     * the first attribute (in ascending order), on equality the second
     * attribute is used, and so on.
     *)
      self # get_somehow_sorted_records
	(List.map 
	   (fun n -> 
	     let t = try List.assoc n (typedef_of_scheme dbscheme)
	             with Not_found -> 
		       failwith ("get_sorted_records: unknown attribute")
	     in 
	     (n, Field.compare_fields t )) 
	   names)


    method get_somehow_sorted_records names_and_cmp =
      let pred a b =
	try
	  List.iter
	    (fun (name,cmp) ->
	      try
		let a_value = field a name in
		try
		  let b_value = field b name in
		  let result = cmp a_value b_value in
		  if result <> 0 then
		    raise (Result (result < 0))
		with
		  Not_found ->
		    raise (Result true)
	      with
		Not_found ->
		  raise (Result false))
	    names_and_cmp;
	  false      (* a = b *)
	with
	  Result r -> r
      in
      Sort.list pred (Array.to_list contents)


    method select pred =
      (* returns all records for which the predicate 'pred' is true *)
      let result = ref [] in
      Array.iter
	(fun r -> if pred r then result := r :: !result)
	contents;
      !result

  end
