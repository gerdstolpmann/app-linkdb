(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Field

let tables = ref [];;
let cats = ref [];;
let within_transaction = ref false;;

Cgi.register_init (fun () -> 
		     tables := [];
		     cats := [];
		     within_transaction := false) ;;

let register_table tab_name table =
  if !within_transaction then
    failwith("Base.register_table: already within transaction");
  Logging.write Logging.Startup ("Registering table " ^ tab_name);
  if List.mem_assoc tab_name !tables then
    failwith ("register_table: '" ^ tab_name ^ "' already registered");
  tables := (tab_name,table) :: !tables
;;


let get_table tab_name =
  let tab = List.assoc tab_name !tables in
  tab
;;


let get_scheme tab_name =
  let tab = get_table tab_name in
  tab # scheme
;;


let register_catalogue_directly cat_name cat is_multiple =

  Logging.write Logging.Startup ("Registering cat " ^ cat_name);

  if List.mem_assoc cat_name !cats then
    failwith ("register_catalogue_directly: '" ^ cat_name ^ 
	      "' already registered");

  let cat_kv = List.map (fun (k,(v,_)) -> k,v) cat in
  let default = 
    List.flatten (List.map (fun (k,(_,d)) -> if d then [k] else []) cat) in

  cats := (cat_name,
	   Field.mk_selection
	     cat_name
	     is_multiple
	     (fun () -> cat_kv)
	     (fun () -> default)) 
      :: !cats
;;


let register_catalogue_exd cat_name table_name key_att_name value_att_name
    is_multiple default =

  Logging.write Logging.Startup ("Registering cat " ^ cat_name);

  if List.mem_assoc cat_name !cats then
    failwith ("register_catalogue_exd: '" ^ cat_name ^ 
	      "' already registered");
  
  cats := (cat_name,

	   Field.mk_selection

	     cat_name

	     is_multiple

	     (fun () -> 
	       let tab = 
		 try
		   get_table table_name
		 with
		   Not_found -> failwith ("Catalogue '" ^ cat_name ^ 
					  "' refers to non-existing table '" ^
					  table_name ^ "'")
	       in
	       let recs = tab # select (fun _ -> true) in
	       let kv_list =
		 List.flatten
		   (List.map
		      (fun r ->
			try
			  [ dest_line(r # field key_att_name),
			    dest_line(r # field value_att_name) ]
			with
			  No_content -> [])
		      recs) in
	       Sort.list
		 (fun (a,_) (b,_) -> a <= b)
		 kv_list)

	     (fun () -> default))

      :: !cats


let register_catalogue_imd cat_name table_name key_att_name value_att_name
    is_multiple default =

  Logging.write Logging.Startup ("Registering cat " ^ cat_name);

  if List.mem_assoc cat_name !cats then
    failwith ("register_catalogue_imd: '" ^ cat_name ^ 
	      "' already registered");
  
  cats := (cat_name,

	   Field.mk_selection

	     cat_name

	     is_multiple

	     (fun () -> 
	       let tab = 
		 try
		   get_table table_name
		 with
		   Not_found -> failwith ("Catalogue '" ^ cat_name ^ 
					  "' refers to non-existing table '" ^
					  table_name ^ "'")
	       in
	       let recs = tab # select (fun _ -> true) in
	       let kv_list =
		 List.flatten
		   (List.map
		      (fun r ->
			try
			  [ dest_line(r # field key_att_name),
			    dest_line(r # field value_att_name) ]
			with
			  No_content -> [])
		      recs) in
	       Sort.list
		 (fun (a,_) (b,_) -> a <= b)
		 kv_list)

	     (fun () -> 
	       let tab = 
		 try
		   get_table table_name
		 with
		   Not_found -> failwith ("Catalogue '" ^ cat_name ^ 
					  "' refers to non-existing table '" ^
					  table_name ^ "'")
	       in
	       let recs = tab # select (fun _ -> true) in
	       let k_list =
		 List.flatten
		   (List.map
		      (fun r ->
			if r # field default = null then
			  []
			else
			  try
			    [ dest_line(r # field key_att_name) ]
			  with
			    No_content -> [])
		      recs)
	       in
	       k_list))

      :: !cats


let get_selection cat_name =
  List.assoc cat_name !cats
;;


let get_record id =
  let tab_name = Index.lookup_id id in  (* may raise Not_found *)
  try
    let tab = get_table tab_name in
    try
      tab # get_record id
    with
      Not_found ->
	failwith "Base.get_record: index.db is not up to date"
  with
    Not_found ->
      failwith ("Base.get_record: table '" ^ tab_name ^ "' is not registered")
;;


let begin_transaction() =
  Lock.require_write_permission();

  if !within_transaction then
    failwith("Base.begin_transaction: already within transaction");

  List.iter
    (fun (_,tab) ->
      tab # begin_transaction())
    !tables;

  within_transaction := true
;;


let commit_transaction() =
  Lock.require_write_permission();

  if not !within_transaction then
    failwith("Base.commit_transaction: not within transaction");

  List.iter
    (fun (_,tab) ->
      if tab # is_modified() then 
	tab # write_database())
    !tables;

  List.iter
    (fun (_,tab) ->
      if tab # is_modified() then 
	tab # commit_transaction())
    !tables;

  within_transaction := false
;;


let abort_transaction() =
  failwith "abort_transaction: not implemented";

  (* The following is not correct because 
   * (i) You can get tables using "get_table" and store them elsewhere
   * (ii) already loaded catalogues are not reset
   *
   * It is unclear what should happen at all...
   *)

  if not !within_transaction then
    failwith("Base.abort_transaction: not within transaction");
  
  tables := [];
  within_transaction := false
;;
