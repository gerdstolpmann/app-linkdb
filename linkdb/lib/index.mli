(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* The index manages database-wide ID numbers. The index is a table of
 * IDs and the corresponding database tables of the records.
 *)

(* Note:
 * Currently, all of the functions below operate directly on the file
 * "index.db", there is no cache. This means that these functions are
 * rather slow.
 *)


val fetch_id : string -> int
  (* Get a new ID that is unique in the whole database. The argument is the
   * name of the table where the ID will be used.
   * This function requires a write lock on the database.
   *)

val release_id : int -> unit
  (* Release the passed ID number.
   * This function requires a write lock on the database.
   *)

val lookup_id : int -> string
  (* Find out the table where the passed ID is used.
   * This function requires a read lock on the database.
   *)
