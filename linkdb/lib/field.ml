(* $Id$
 * ----------------------------------------------------------------------
 * 
 *)

(* PLAN:
 * Typed fields have some advantages:
 * - reasonable defaults for sorting and searching depending on the type
 * - can form predicates in a type-specific manner
 * There should be:
 * - for every field description, there should be functions that
 *   + represent the field as human-readable string (there may be some 
 *     default functions)
 *   + convert from and to db representation
 *   + check user input (detects ill-formed strings)
 *   + generate an HTML form element
 * Especially selection lists should be designed carefully. Such lists
 * must be extensible and shrinkable.
 *
 * There should be a type "select_predicate" that is an EXTENSIVE description
 * of the criterion (currently we only have intensive predicates, i.e.
 * functions, for "select").
 *
 * Existing tables must be converted into the new format.
 *)

(* Imported:
 * - Common.User_error
 * - Common.escape_html
 * - Common.hex_digest
 *)

open Str
open Common

(****************** Type definitions *********************)

type selection = 
    { catalogue : string;
      multiple : bool;
      get_values : unit -> (string * string) list;
                  (* pairs (internal_value, external_value) *)
      get_default : unit -> string list
    } 
;;


type field_type =
    T_line
  | T_password
  | T_textbox
  | T_hyperlink
  | T_int
  | T_select of selection
  | T_date
  | T_constrained of (field_type * (field -> unit))

and field = 
    Line of string                 (* line (without '\n' characters) *)
  | Password of string             (* as Line, but stored differently *)
  | Textbox of string              (* multiline box *)
  | Hyperlink of string list       (* e.g. http://somewhere/somedir *)
  | Int of int
  | Select of (selection * string list)  (* list: set of internal values *)
  | Date of int                    (* days since Jan 1, 1970 = 0 *)
  | Null                           (* Field is not set *)
  | Unchecked of (field_type * field_type * string)
      (* unchecked value created by 'mk_unchecked_field'. The first type is 
       * the type given to this function; the second type is the base type
       * (i.e. not T_constrained). The string is the string passed to
       * 'mk_unchecked_field'
       *)
;;


exception No_content;;

exception Unchecked_content of string;;

type out_config_spec =
    Out_textbox of bool
  | Out_date of bool
  | Out_hyperlink of string
  | Out_hyperlink_self
  | Out_select of string
;;

type out_format =
    Html of string
  | Latin1 of string
  | Field of (string * field)
  | Config of ((string * out_config_spec) list * out_format list)
  | List of (string * out_format list)       
;;


(**************** common **************************)

let newline_re = regexp "\n";;


(************* properties of field types *************)

let rec equal_field_types t t' =
  match t with
    (T_line | T_password | T_textbox | T_hyperlink | T_int | T_date) ->
      t = t'
  | T_select sel ->
      begin match t' with
	T_select sel' ->
	  sel == sel'
      |	_ ->
	  false
      end
  | T_constrained (sub_t, checker) ->
      begin match t' with
	T_constrained (sub_t', checker' ) ->
	  checker == checker' & equal_field_types sub_t sub_t'
      |	_ ->
	  false
      end
;;
      


(****************** constructors ********************)


let mk_selection catname is_multiple f_get_values f_get_default =
  (* TODO: check that no ':' characters are used in the internal 
   * representations
   *)
  { catalogue = catname;
    multiple = is_multiple;
    get_values = f_get_values;
    get_default = f_get_default;
  } 
;;


(* If a mk_* function cannot convert its arguments, a User_error is raised,
 * because the arguments are usually entered by a user
 *)


let is_leap_year lyear =
  (* Find out if the year is a leap-year *)
  (lyear mod 4 = 0) &
  ((lyear mod 400 = 0) or not (lyear mod 100 = 0))
;;


let mk_date day month year =
  (* form a Date value *)

  (* for all hysterical managers: this routine is Y2K-compliant *)

  (* The following is absolutely senseless before 1582. In the countries
   * that introduced the Gregorian calendar first, some days (Oct 4 to
   * Oct 15) are missing in 1582. So we assume that October 16, 1582 is
   * the first day that makes sense.
   *)

  if year < 1582 or (year = 1582 & (month < 10 or (month = 10 & day < 16)))
  then
    user_error "Date is before introduction of the Gregorian calendar";

  let month_length =
    [| 31;
       if is_leap_year year then 29 else 28;
       31;
       30;
       31;
       30;
       31;
       31;
       30;
       31;
       30; 
       31 |] in

  if month < 1 or month > 12 then
    user_error "Date has bad month";

  let len = month_length.( month-1 ) in

  if day < 1 or day > len then
    user_error "Date has bad day";

  if year >= 1970 then begin
    (* positive day offset *)
    let rec days_in y =
      if y >= 1970 then begin
	let y_days = if is_leap_year y then 366 else 365 in
	y_days + days_in (y-1)
      end
      else 0
    in
    let rec days_until_month m =
      if m >= 1 then begin
	let m_days = month_length.(m-1) in
	m_days + days_until_month (m-1)
      end
      else 0
    in
    Date (days_in (year-1) + days_until_month (month-1) + day - 1)
  end
  else begin
    (* negative day offset, e.g. Dec 31, 1969 is -1 *)
    let rec days_in y =
      if y < 1970 then begin
	let y_days = if is_leap_year y then 366 else 365 in
	y_days + days_in (y+1)
      end
      else 0
    in
    let rec days_since_month m =
      if m <= 12 then begin
	let m_days = month_length.(m-1) in
	m_days + days_since_month (m+1)
      end
      else 0
    in
    Date (- days_in (year+1) - days_since_month (month+1) - (len - day) - 1)
  end
;;


let mk_today() =
  Date(truncate(Unix.time() /. 86400.0))
;;


let digit_seq_re = regexp "[0-9]+";;
let alpha_seq_re = regexp "[a-zA-Z]+";;

let mk_date_from_string s =
  let is_digit = function '0'..'9'->true | _ -> false in
  let len = String.length s in
  let rec get_sequences re k =
    try
      let k' = search_forward re s k in
      let d = matched_string s in
      d :: get_sequences re (match_end())
    with
      Not_found -> []
  in
  let get_digit_sequences = get_sequences digit_seq_re in
  let get_alpha_sequences = get_sequences alpha_seq_re in
  let digit_sequences = List.map int_of_string (get_digit_sequences 0) in
  let alpha_sequences = get_alpha_sequences 0 in
  let rec find_unambigous p l =
    match l with
      [] -> raise Not_found
    | x::l' ->
	if p x then begin
	  if List.exists p l' then raise Not_found else x
	end
	else find_unambigous p l'
  in
  let day =
    try
      find_unambigous (fun n -> n >= 1 & n <= 31) digit_sequences
    with
      Not_found -> user_error "Cannot extract date from your input"
  in
  let year =
    try
      find_unambigous (fun n -> n >= 1582 & n <= 9999) digit_sequences
    with
      Not_found -> user_error "Cannot extract date from your input"
  in
  let month_names =
    [ "january"; "february"; "march"; "april"; "may"; "june"; 
      "july"; "august"; "september"; "septembre"; "november"; "novembre";
      "december"; "decembre" ] in
  let month_map =
    [ "jan", 1;
      "feb", 2;
      "mar", 3;
      "apr", 4;
      "may", 5;
      "jun", 6;
      "jul", 7;
      "aug", 8;
      "sep", 9;
      "oct", 10;
      "nov", 11;
      "dec", 12 ] in
  let get_month s =
    let s_lower = String.lowercase s in
    let l = String.length s in
    if l >= 3 then begin
      let full_name =
	find_unambigous 
	  (fun name -> 
	    let l_name = String.length name in
	    (l <= l_name) & (String.sub name 0 l = s_lower))
	  month_names 
      in
      List.assoc (String.sub full_name 0 3) month_map
    end
    else
      raise Not_found
  in
  let is_month s =
    try let _ = get_month s in true with Not_found -> false in
  let month =
    try
      get_month (find_unambigous is_month alpha_sequences)
    with
      Not_found -> user_error "Cannot extract date from your input"
  in
  mk_date day month year
;;


let mk_line s =
  if s = "" or not (String.contains s '\n') then
    Line s
  else
    user_error "String contains newline characters which is not allowed here"
;;


let mk_password s =
  Password (hex_digest s)
;;


let mk_textbox s =
  Textbox s
;;


let form_http_re = Str.regexp "^http://";;
let form_ftp_re = Str.regexp "^ftp://";;
let form_mailto_re = Str.regexp "^mailto:[^@]+@[^@]+$";;

let mk_hyperlink s0 =
  let is_url s =
    Str.string_match form_http_re s 0 or
    Str.string_match form_ftp_re s 0 or
    Str.string_match form_mailto_re s 0 
  in
  let sl = Str.split (regexp "[ \\t]*,[ \\t]*") s0 in
  Hyperlink
    (List.map
       (fun s ->
	 if is_url s then
	   s
	 else
	   user_error "Ill-formed hyperlink")
       sl)
;;


let mk_int_from_string i =
  try
    Int (int_of_string i)
  with
    _ -> user_error "Ill-formed integer number"
;;


let mk_int i = Int i;;


let mk_select (T_select t) internal_value =
  let l = List.length internal_value in
  if not t.multiple & l <> 1 then
    user_error "Exactly one choice must be selected";
  let vlist = t.get_values() in
  if not (List.for_all (fun n -> List.mem_assoc n vlist) internal_value) then
    user_error "bad value";
  Select (t,internal_value)
;;


let mk_select_default (T_select t) =
  Select (t, t.get_default())
;;


let colon_sep = regexp ":";;

let rec mk_field ftype user_input =
  (* checks if 'user_input' matches with the description 'ftype' *)
  match ftype with
    T_line      -> mk_line user_input
  | T_textbox   -> mk_textbox user_input
  | T_password  -> mk_password user_input
  | T_hyperlink -> mk_hyperlink user_input
  | T_int       -> mk_int_from_string user_input
  | T_select s  -> mk_select ftype (split colon_sep user_input)
  | T_date      -> mk_date_from_string user_input
  | T_constrained (ftype', checker) ->
      let f = mk_field ftype' user_input in
      checker f;
      f
;;


let rec get_base_type t =
  match t with
    T_constrained(t',_) -> get_base_type t'
  | _ -> t
;;


let mk_unchecked_field ftype user_input =
  (* Creates an unchecked field value. It can be transformed to a checked
   * value by 'check_field'. Most functions below cannot handle with unchecked
   * values, but especially the html_* functions can.
   *)
  Unchecked(ftype, get_base_type ftype, user_input)
;;


let check_field f =
  match f with
    Unchecked(ftype,btype,user_input) -> mk_field ftype user_input
  | other                             -> other
;;


let null = Null;;

(********************** destroy ******************)


(* no dest_password ! *)

let dest_line =
  function Line l -> l 
	 | Unchecked(_,T_line,s) -> raise (Unchecked_content s)
         | Null -> raise No_content 
         | _ -> failwith "dest_line";;

let dest_textbox =
  function Textbox t -> t 
	 | Unchecked(_,T_textbox,s) -> raise (Unchecked_content s)
         | Null -> raise No_content 
         | _ -> failwith "dest_textbox";;

let dest_hyperlink =
  function Hyperlink h -> h 
	 | Unchecked(_,T_hyperlink,s) -> raise (Unchecked_content s)
         | Null -> raise No_content 
         | _ -> failwith "dest_hyperlink";;

let dest_int =
  function Int i -> i 
	 | Unchecked(_,T_int,s) -> raise (Unchecked_content s)
         | Null -> raise No_content 
         | _ -> failwith "dest_int";;

let dest_select =
  (* get internal values *)
  function Select (_,s) -> s 
	 | Unchecked(_,T_select _,s) -> raise (Unchecked_content s)
         | Null -> raise No_content 
         | _ -> failwith "dest_select";;

let dest_date =
  (* get triple (day,month,year) *)
  function 
      Date d -> begin
	let rec get_year refyear remaining_days =
	  if remaining_days >= 0 then begin
	    let d_in_refyear = if is_leap_year refyear then 366 else 365 in
	    if remaining_days >= d_in_refyear then
	      get_year (refyear+1) (remaining_days - d_in_refyear)
	    else
	      refyear, remaining_days
	  end
	  else
	    let refyear' = refyear - 1 in
	    let d_in_refyear' = if is_leap_year refyear' then 366 else 365 in
	    get_year refyear' (remaining_days + d_in_refyear')
	in
	let (year, days_in_year) = get_year 1970 d in
	let month_length =
	  [| 31;
	    if is_leap_year year then 29 else 28;
	    31;
	    30;
	    31;
	    30;
	    31;
	    31;
	    30;
	    31;
	    30; 
	    31 |] in
	let rec get_month m remaining_days =
	  let l_m = month_length.( m-1 ) in
	  if remaining_days < l_m then
	    m, remaining_days
	  else
	    get_month (m+1) (remaining_days - l_m)
	in
	let month, day = get_month 1 days_in_year in
	(day+1, month, year)
      end
    | Unchecked(_,T_date,s) -> raise (Unchecked_content s)
    | Null -> raise No_content 
    | _ -> failwith "dest_date"
;;


let eq_password f pw =
  let pw_hex = hex_digest pw in
  match f with
    Password pw'_hex -> pw_hex = pw'_hex
  | Null -> raise No_content 
  | _ -> failwith "eq_password"
;;


let rec field_matches_type f t =
  match t with 
    T_line      -> (match f with Line _ -> true | Null -> true | _ -> false)
  | T_password  -> (match f with Password _ -> true | Null -> true | _ -> false)
  | T_textbox   -> (match f with Textbox _ -> true | Null -> true | _ -> false)
  | T_hyperlink -> (match f with Hyperlink _ -> true | Null -> true | _ -> false)
  | T_int       -> (match f with Int _ -> true | Null -> true | _ -> false)
  | T_date      -> (match f with Date _ -> true | Null -> true | _ -> false)
  | T_constrained (t', checker) ->
      (try checker f; true with _ -> false) &
      field_matches_type f t'
  | T_select seldef  -> 
      begin match f with
	Select (selref,sl) -> 
	  let vals = seldef.get_values() in
	  if (not seldef.multiple) & List.length sl <> 1 then
	    false
	  else
	    List.for_all
	      (fun s -> List.mem_assoc s vals)
	      sl
      | Null -> true 
      | _ ->
	  false
      end
;;




(******************** dates ************************)

let add_delta f n =
  match f with
    Date d -> Date (d + n)
  | Null -> raise No_content
  | _ -> failwith "add_delta"


let get_delta f1 f2 =
  match (f1,f2) with
    (Date d), (Date d') -> d - d'
  | Null, _ -> raise No_content
  | _, Null -> raise No_content
  | _, _    -> failwith "get_delta"


(******************** HTML generation ****************)

(* Functions html_output_*: these generate simple text sections.
 * Functions html_input_*: these generate form elements (interactors)
 *)

(* Notes:
 * - no html_output_password
 * - html_input_password does not allow to specify a default value
 *)

let html_output_line = 
  function 
      Line s -> escape_html s
    | Unchecked(_,T_line, s) -> escape_html s
;;


let html_output_textbox multiline = 
  (* multiline: if true, newlines are replaced by <br> elements *)
  function
      Textbox s ->
	if multiline then
	  global_replace newline_re "<br>\n" (escape_html s)
	else
	  escape_html s
    | Unchecked(_,T_textbox, s) ->
	if multiline then
	  global_replace newline_re "<br>\n" (escape_html s)
	else
	  escape_html s
;;


let html_output_int =
  function
      Int i -> string_of_int i
    | Unchecked(_,T_int, i) -> escape_html i
;;


let months = 
  [| "Jan"; "Feb"; "Mar"; "Apr"; "May"; "Jun"; "Jul"; "Aug"; "Sep";
     "Oct"; "Nov"; "Dec" |]
;;


let html_output_date unknown =
  (* unknown: if true, 1/1/1970 is printed as "unknown date" *)
  function 
      Date d ->
	if unknown & d=0 then
	  "unknown date"
	else
	  let (day,month,year) = dest_date (Date d) in
	  string_of_int day ^ " " ^ months.(month-1) ^ " " ^ string_of_int year
    | Unchecked(_,T_date, s) ->
	escape_html s
;;
  

let html_output_hyperlink html_text =
  (* html_text: an HTML expression that is marked as hyperlink *)
  function
      Hyperlink h ->
	String.concat
	  ", "
	  (List.map
	     (fun h0 ->
	       "<a href=\"" ^ escape_html h0 ^ "\" target=\"_main\">" ^ html_text h0 ^ "</a>")
	     h)
    | Unchecked(_,T_hyperlink, s) ->
	let h = Str.split (regexp "[ \\t]*,[ \\t]*") s in
	String.concat
	  ", "
	  (List.map
	     (fun h0 ->
	       "<a href=\"" ^ escape_html h0 ^ "\" target=\"_main\">" ^ html_text h0 ^ "</a>")
	     h)
;;


let html_output_select html_separator =
  (* html_separator: HTML text that separates multiple choices *)
  function
      Select (t,slist) ->
	let value_map = t.get_values() in
	String.concat
	  html_separator
	  (List.map
	     (fun name ->
	       escape_html(List.assoc name value_map))
	     slist)
    | Unchecked (_,T_select t, s) ->
	(* assume that 's' is a colon-separated list of keys *)
	let value_map = t.get_values() in
	let keys = split colon_sep s in
	let values =
	  List.flatten
	    (List.map 
	       (fun k ->
		 try
		   [ escape_html(List.assoc k value_map) ]
		 with
		   Not_found -> [])
	       keys) in
	String.concat html_separator values
;;


let html_output fmtlist =
  let rec find_config conf name p =
    match conf with
      [] -> failwith "internal error"
    | (n,c)::conf' ->
	if n = name or n = "*" then
	  try p c with Not_found -> find_config conf' name p
	else
	  find_config conf' name p
  in

  let rec output sep conf fmtlist =
    String.concat 
      sep
      (List.map
	 (function
	     Html text -> text
	   | Latin1 text -> Common.escape_html text
	   | Field (name, f) -> 
	       begin match f with
		 Line s -> html_output_line f
	       | Unchecked(_,T_line,s) -> html_output_line f
	       | Password s -> "******"
	       | Unchecked(_,T_password,s) -> "******"
	       | Textbox s -> 
		   let b = find_config conf name
		             (function Out_textbox b -> b 
                                     | _ -> raise Not_found) in
		   html_output_textbox b f
	       | Unchecked(_,T_textbox, s) -> 
		   let b = find_config conf name
		             (function Out_textbox b -> b 
                                     | _ -> raise Not_found) in
		   html_output_textbox b f
	       | Hyperlink s ->
		   let h = find_config conf name
		             (function Out_hyperlink h -> (fun _ -> h)
			             | Out_hyperlink_self -> escape_html
			             | _ -> raise Not_found) in
		   html_output_hyperlink h f
	       | Unchecked(_,T_hyperlink,s) ->
		   let h = find_config conf name
		             (function Out_hyperlink h -> (fun _ -> h)
			             | Out_hyperlink_self -> escape_html
			             | _ -> raise Not_found) in
		   html_output_hyperlink h f
	       | Int k -> html_output_int f
	       | Unchecked(_,T_int,_) -> html_output_int f
	       | Select (seldef, sl) ->
		   let sep = find_config conf name 
		               (function Out_select sep -> sep
				       | _ -> raise Not_found) in
		   html_output_select sep f
	       | Unchecked(_,T_select _,_) ->
		   let sep = find_config conf name 
		               (function Out_select sep -> sep
				       | _ -> raise Not_found) in
		   html_output_select sep f
	       | Date d ->
		   let b = find_config conf name
		             (function Out_date b -> b
			             | _ -> raise Not_found) in
		   html_output_date b f
	       | Unchecked(_,T_date,_) ->
		   let b = find_config conf name
		             (function Out_date b -> b
			             | _ -> raise Not_found) in
		   html_output_date b f
	       | Null -> ""
	       end
	   | Config (conf', subfmtlist) ->
	       output "" (conf' @ conf) subfmtlist
	   | List (separator, subfmtlist) ->
	       output separator conf subfmtlist)
	 fmtlist)
  in
  let default_config =
    [ "*", Out_textbox true; 
      "*", Out_date true; 
      "*", Out_hyperlink_self;
      "*", Out_select ", " ]
  in
  output "" default_config fmtlist
;;


let html_input_line in_name width f =
  (* in_name:    name of the input element
   * width:      visible length of the text box 
   *)
  let v = 
    match f with
      Null -> ""
    | Line s -> escape_html s
    | Unchecked(_,T_line,s) -> escape_html s
  in
  "<input type=text name=\"" ^ escape_html in_name ^ "\" value=\"" ^ 
  v ^ "\" size=\"" ^ string_of_int width ^ "\">"
;;


let html_input_textbox in_name width height f =
  let v = 
    match f with
      Null -> ""
    | Textbox s -> escape_html s
    | Unchecked(_,T_textbox,s) -> escape_html s
  in
  "<textarea name=\"" ^ escape_html in_name ^ "\" cols=\"" ^
  string_of_int width ^ "\" rows=\"" ^ string_of_int height ^ 
  "\" wrap=off>" ^
  v ^
  "</textarea>"
;;


let html_input_password in_name width f =
  (* Does not use <input type=password> because passwords are transferred
   * in clear text, so everybody can watch your password someway 
   *)
  match f with
    Unchecked(_,T_password, s) ->
      html_input_line in_name width (Unchecked(T_line,T_line,s))
  | _ ->
      html_input_line in_name width Null
;;


let html_input_int in_name javascript width f =
  (* javascript: enables an "onchange" input handler *)
  let js = "var r = 0; for (i=0; i<this.value.length; ++i) { var s = this.value.substr(i,1); r |= (s != '0' && s != '1' && s != '2' && s != '3' && s != '4' && s != '5' && s != '6' && s != '7' && s != '8' && s != '9'); }; if (r) { this.value = ''; alert('Please enter a number!'); this.focus(); }" in
  
  let v = 
    match f with
      Null -> ""
    | Int i -> string_of_int i
    | Unchecked(_,T_int, s) -> escape_html s
  in

  "<input type=text name=\"" ^ escape_html in_name ^ "\" value=\"" ^ 
  v ^ "\" size=\"" ^ string_of_int width ^ "\" " ^
  (if javascript then " onchange=\"" ^ js ^ "\" " else "") ^ 
  ">"
;;  


let html_input_date in_name (javascript:bool) width d =
  (* javascript: not yet implemented *)

  let s = 
    match d with
      Null -> ""
    | Date _ -> html_output_date false d 
    | Unchecked(_,T_date,s) -> s
  in

  "<input type=text name=\"" ^ escape_html in_name ^ "\" value=\"" ^ 
  escape_html s ^ "\" size=\"" ^ string_of_int width ^ "\" " ^
  (if javascript then "" else "") ^ 
  ">"
;;


let html_input_hyperlink in_name javascript width f =
  (* javascript: not yet implemented *)

  let v = 
    match f with
      Null -> ""
    | Hyperlink h -> escape_html (String.concat ", " h)
    | Unchecked(_,T_hyperlink,s) -> escape_html s
  in

  "<input type=text name=\"" ^ escape_html in_name ^ "\" value=\"" ^ 
  v ^ "\" size=\"" ^ string_of_int width ^ "\" " ^
  (if javascript then "" else "") ^ 
  ">"
;;


let html_input_select_asbox in_name height f =
  (* uses <select> *)

  let mk_box t slist =
    (* Note: ignores illegal entries in 'slist' *)
    "<select name=\"" ^ escape_html in_name ^ "\" size=\"" ^ 
    string_of_int height ^ "\"" ^ 
    (if t.multiple then " multiple" else "") ^
    ">" ^
    String.concat
      "\n" 
      (List.map
	 (fun (v_int, v_ext) ->
	   "<option value=\"" ^ escape_html v_int ^ "\"" ^
	   (if List.mem v_int slist then " selected" else "") ^
	   ">" ^ escape_html v_ext)
	 (t.get_values())) ^
    "</select>"
  in
  match f with
    Select (t,slist) -> mk_box t slist
  | Unchecked (_,T_select t, s) ->
      let slist = split colon_sep s in
      mk_box t slist
;;

(* add: html_input_select_asbuttons: uses multiple "radio" or "checkbutton" *)


(******************* sorting and searching **********)

let set_compare s1 s2 =
  (* returns -1 if s1 is subset of s2, 
   *         +1 if s2 is subset of s1,
   *          0 if s1 is set-equal to s2,
   * raises Not_found if s1 and s2 are incomparable
   *)
  let is_subset z1 z2 =
    List.for_all (fun x1 -> List.mem x1 z2) z1
  in
  if is_subset s1 s2 then begin
    if is_subset s2 s1 then 0 else (-1)
  end
  else
    if is_subset s2 s1 then
      1
    else
      raise Not_found
;;


let card_compare s1 s2 =
  let n1 = List.length s1 in
  let n2 = List.length s2 in
  if n1 = n2 then
    raise Not_found
  else
    n1 - n2
;;


let compare_fields ftype f1 f2 =
  try
    begin match get_base_type ftype with
      T_line      -> compare (String.lowercase(dest_line f1)) 
	                     (String.lowercase(dest_line f2))
    | T_password  -> failwith "compare_fields: cannot compare passwords"
    | T_textbox   -> compare (dest_textbox f1) (dest_textbox f2)
    | T_hyperlink -> compare (String.concat ", " (dest_hyperlink f1)) 
	                     (String.concat ", " (dest_hyperlink f2))
    | T_int       -> compare (dest_int f1) (dest_int f2)
    | T_select s  -> 
	begin
	  let s1 = dest_select f1 in
	  let s2 = dest_select f2 in
	  begin
	    try
	      card_compare s1 s2
	    with
	      Not_found ->
		  (* Last criterion: sort s1 and s2 and compare by characters *)
		let s1_sorted = Sort.list ( <= ) s1 in
		let s2_sorted = Sort.list ( <= ) s2 in
		compare 
		  (String.concat "," s1_sorted) 
		  (String.concat "," s2_sorted)
	  end
	end
    | T_date      -> get_delta f1 f2
    | T_constrained (_,_) -> failwith "internal error"
    end
  with
    No_content ->
      begin match (f1,f2) with
	(Null, Null) -> 0
      |	(Null, _)    -> 1
      |	(_, Null)    -> (-1)
      |	(_,_)        -> failwith "compare_fields"
      end
  | Unchecked_content _ -> failwith "compare_fields"
;;


let string_of_field ftype f =
  try
    match get_base_type ftype with
      T_line      -> dest_line f
    | T_password  -> failwith "string_of_field: cannot be applied to passwords"
    | T_textbox   -> dest_textbox f
    | T_hyperlink -> String.concat ", " (dest_hyperlink f)
    | T_int       -> string_of_int(dest_int f)
    | T_date      -> if f = null then raise No_content 
                                 else html_output_date false f
    | T_select s  -> 
	let slist = dest_select f in
	String.concat "," slist
    | T_constrained (_,_) -> failwith "internal error"
  with
    No_content -> ""
  | Unchecked_content _ -> failwith "string_of_field"
;;


let debug_print_field f =
  match f with
    Line s -> ("Line(" ^ s ^ ")")
  | Password s -> ("Password(" ^ s ^ ")")
  | Textbox s -> ("Textbox(" ^ s ^ ")")
  | Hyperlink s -> ("Hyperlink(" ^ String.concat ", " s ^ ")")
  | Int k -> ("Int(" ^ string_of_int k ^ ")")
  | Select (seldef, sl) ->
      "Select(" ^  String.concat ":" sl ^ ")"
  | Date k -> ("Date(" ^ string_of_int k ^ ")")
  | Null -> "Null"
  | Unchecked (ft, ftbase, s) -> ("Unchecked(" ^ s ^ ")")
;;


let rec debug_print_field_type ft =
  match ft with
    T_line -> "T_line"
  | T_password -> "T_password"
  | T_textbox -> "T_textbox"
  | T_hyperlink -> "T_hyperlink"
  | T_int -> "T_int"
  | T_select sel ->
      let vals = sel.get_values() in
      let d = sel.get_default() in
      "T_select(" ^
      String.concat "," (List.map (fun (s1,s2) -> s1 ^ "/" ^ s2) vals) ^
      "; multiple=" ^ 
      (if sel.multiple then "true" else "false") ^ 
      "; default=" ^ 
      String.concat ":" d ^ 
      ")"
  | T_date -> "T_date"
  | T_constrained (ft', checker) ->
      "T_constrained(" ^ debug_print_field_type ft' ^ ")"
;;


(******************** load and store ****************)

let bar_newline_re = regexp "|n";;
let bar_itself_re  = regexp "|b";;

let bar_re = regexp "|";;

let load_textbox s =
  Textbox 
    (global_replace bar_itself_re "|" (global_replace bar_newline_re "\n" s))
;;


let store_textbox s =
  global_replace newline_re "|n" (global_replace bar_re "|b" s)
;;


let load_date d =
  try
    Date (int_of_string d)
  with
    _ -> failwith "load_date"
;;


let load_field ftype db_input =
  try
    begin match get_base_type ftype with
      T_line      -> mk_line db_input
    | T_password  -> Password db_input  (* bypass mk_password! *)
    | T_textbox   -> load_textbox db_input
    | T_hyperlink -> mk_hyperlink db_input
    | T_int       -> mk_int_from_string db_input
    | T_select s  -> mk_select ftype (split colon_sep db_input)
    | T_date      -> load_date db_input
    | T_constrained (_,_) -> failwith "internal error"
    end
  with
    User_error s ->
      Logging.write Logging.Err ("Bad line: " ^ db_input);
      failwith ("While loading from database table: " ^ s)
;;


let store_field ftype value =
  try
    begin match get_base_type ftype with
      T_line      -> (match value with Line s -> s | _ -> raise Not_found)
    | T_password  -> (match value with Password s -> s | _ -> raise Not_found)
    | T_textbox   -> (match value with
                        Textbox s -> store_textbox s
                      |	_ -> raise Not_found)
    | T_hyperlink -> (match value with 
                        Hyperlink s -> String.concat "," s
                      | _ -> raise Not_found)
    | T_int       -> (match value with
                        Int s -> string_of_int s
                      |	_ -> raise Not_found)
    | T_select s  -> (match value with
                        Select (_,slist) -> String.concat ":" slist
                      |	_ -> raise Not_found)
    | T_date      -> (match value with
                        Date s -> string_of_int s
                      |	_ -> raise Not_found)
    | T_constrained(_,_) -> failwith "internal error"
    end
  with
    Not_found -> failwith "Type mismatch while storing into database table"
;;
