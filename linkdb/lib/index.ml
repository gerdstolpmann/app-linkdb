(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* Format of the "index.db" file:
 *
 * First line: maxid (biggest ID used) in 10 characters
 * On the next lines, there are ':' separated pairs of IDs and table names
 *)

(* Note:
 * There is currently no cache for the "index.db" file. We would need improved
 * Lock semantics to check that the cache is up to date. (The cache is only
 * valid if the current lock is still the same as the lock at the moment
 * the cache has been filled.)
 *)


open Common;;

let index_db() = !basedir ^ "/index.db";;

let tables = ref [];;

let idlist = ref [];;

let maxid = ref 0;;

(* !idlist_loaded implies !maxid_known, but not vice versa *)


let findout_maxid () =
  (* requires read permissions *)
  let f = open_in (index_db()) in
  try
    maxid := int_of_string (input_line f);
    close_in f
  with
    any ->
      close_in f;
      raise any
;;


let line_re = Str.regexp "^\\([0-9]+\\):\\(.*\\)$";;

let read_idlist() =
  (* requires read permissions *)
  let f = open_in (index_db()) in
  try
    maxid := int_of_string (input_line f);
    while true do
      let line = input_line f in
      if Str.string_match line_re line 0 then begin
	let id = int_of_string(Str.matched_group 1 line) in
	let tabname = Str.matched_group 2 line in
	let tabname' =    (* try to get reference to the same table name *)
	  try
	    List.assoc tabname !tables
	  with
	    Not_found -> 
	      tables := (tabname,tabname) :: !tables;
	      tabname
	in
	idlist := (id, tabname') :: !idlist;
      end
      else failwith ("Index: cannot read index")
    done
  with
    End_of_file ->
      close_in f
  | any ->
      close_in f;
      raise any
;;


let write_idlist() =
  (* requires write permissions *)

  let f = open_out (index_db() ^ ".NEW") in
  begin try
    Printf.fprintf f "%010d\n" !maxid;
    List.iter
      (fun (id, tablename) ->
	if id >= 0 then
	  Printf.fprintf f "%d:%s\n" id tablename)
      !idlist;
    close_out f
  with
    any ->
      close_out f;
      raise any
  end;
  Sys.rename (index_db() ^ ".NEW") (index_db())
;;
  

let fetch_id (table_name:string) =
  (* requires write permissions *)

  let append_id table_name =
    (* requires write permissions *)
    findout_maxid();
    let new_id = !maxid + 1 in
    let f = open_out_gen [Open_wronly] 0 (index_db()) in
    begin try
      Printf.fprintf f "%010d\n" new_id;
      close_out f
    with
      any ->
	close_out f;
	raise any
    end;
    let f' = open_out_gen [Open_wronly; Open_append] 0 (index_db()) in
    begin try
      Printf.fprintf f' "%d:%s\n" new_id table_name;
      close_out f'
    with
      any ->
	close_out f';
	raise any
    end;
    maxid := new_id;
    new_id
  in

  Lock.require_write_permission();
  append_id table_name
;;


let release_id id =
  Lock.require_write_permission();

  read_idlist();
  idlist :=
    List.map
      (fun (k,tab) ->
	if k = id then
	  (-1,tab)
	else
	  (k,tab))
      !idlist;
  write_idlist();
  idlist := []
;;


let lookup_id id =
  Lock.require_read_permission();
  read_idlist();
  let t = List.assoc id !idlist in
  idlist := [];
  t
;;
