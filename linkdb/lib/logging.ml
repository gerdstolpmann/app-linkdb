(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* Basic logging functionality *)

open Str;;

type loglevel =
    Debug | Param | Startup | Info | Warn | Err;;

let loglevels = ref [ Param; Info; Warn; Err ];;

let log = ref None ;;

Cgi.register_init (fun () ->
		     match !log with
		       | None -> ()
		       | Some ch -> close_out ch; log := None) ;;


let get_log() =
  match !log with
    | None ->
	let ch =
	  open_out_gen [ Open_wronly; Open_append; Open_creat;
			 Open_text ]
	    0o644
	    (Filename.concat !Config.log_dir "all.log") in
	log := Some ch;
	ch
    | Some ch -> ch


let last_re = regexp "^.*/\\([^/]+\\)$";;

let months = [| "Jan"; "Feb"; "Mar"; "Apr"; "May"; "Jun";
		"Jul"; "Aug"; "Sep"; "Oct"; "Nov"; "Dec" |];;

let do_logging = true;;

let write level s =
  (* write a message to the log file *)
  if do_logging && List.mem level !loglevels then begin
    let lev = 
      List.assoc
	level
	[ Debug, "Debug"; 
	  Param, "Param";
	  Startup, "Startup";
	  Info, "Info";
	  Warn, "Warn";
	  Err, "Err";
	]
    in
    let t = Unix.gmtime (Unix.time()) in
    let t_str = 
      string_of_int (t.Unix.tm_mday) ^ "-" ^ 
      months.(t.Unix.tm_mon) ^ "-" ^ 
      string_of_int (t.Unix.tm_year + 1900) ^ " " ^
      string_of_int (t.Unix.tm_hour) ^ ":" ^ 
      string_of_int (t.Unix.tm_min) ^ ":" ^
      string_of_int (t.Unix.tm_sec)
    in
    let f = get_log() in
    output_string f (t_str ^ " [" ^ Cgi.this_script() ^ "] [" ^ lev ^ "] " ^ s ^ "\n");
    flush f
  end
;;
