(* $Id$
 * ----------------------------------------------------------------------
 * This file is a copy from ocamlcgi release 0.3, see 
 *   http://www.lri.fr/~filliatr/ftp/ocaml/cgi/
 *)

(*
 * ocamlcgi - Objective Caml library for writing CGIs
 * Copyright (C) 1997 Daniel de Rauglaudre, INRIA
 * Copyright (C) 1998 Jean-Christophe FILLIATRE
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License version 2, as published by the Free Software Foundation.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * See the GNU Library General Public License version 2 for more details
 * (enclosed in the file LGPL).
 *)

(*****************************************************)
(* VERSION IN ORIGINAL REPOSITORY                    *)
(* Id: cgi.ml,v 1.5 1999/03/18 16:08:14 filliatr Exp *)
(*****************************************************)

(* Code from wserver.ml, (C) 1997 Daniel de Rauglaudre, INRIA. *)

let hexa_digit x =
  if x >= 10 then Char.chr (Char.code 'A' + x - 10)
  else Char.chr (Char.code '0' + x)
;;

let hexa_val conf =
  match conf with
    '0'..'9' -> Char.code conf - Char.code '0'
  | 'a'..'f' -> Char.code conf - Char.code 'a' + 10
  | 'A'..'F' -> Char.code conf - Char.code 'A' + 10
  | _ -> 0
;;

let decode s =
  let rec need_decode i =
    if i < String.length s then
      match s.[i] with
        '%' | '+' -> true
      | _ -> need_decode (succ i)
    else false
  in
  let rec compute_len i i1 =
    if i < String.length s then
      let i =
        match s.[i] with
          '%' when i + 2 < String.length s -> i + 3
        | _ -> succ i
      in
      compute_len i (succ i1)
    else i1
  in
  let rec copy_decode_in s1 i i1 =
    if i < String.length s then
      let i =
        match s.[i] with
          '%' when i + 2 < String.length s ->
            let v = hexa_val s.[i + 1] * 16 + hexa_val s.[i + 2] in
            s1.[i1] <- Char.chr v; i + 3
        | '+' -> s1.[i1] <- ' '; succ i
        | x -> s1.[i1] <- x; succ i
      in
      copy_decode_in s1 i (succ i1)
    else s1
  in
  let rec strip_heading_and_trailing_spaces s =
    if String.length s > 0 then
      if s.[0] == ' ' then
        strip_heading_and_trailing_spaces
          (String.sub s 1 (String.length s - 1))
      else if s.[String.length s - 1] == ' ' then
        strip_heading_and_trailing_spaces
          (String.sub s 0 (String.length s - 1))
      else s
    else s
  in
  if need_decode 0 then
    let len = compute_len 0 0 in
    let s1 = String.create len in
    strip_heading_and_trailing_spaces (copy_decode_in s1 0 0)
  else s
;;

(* special characters must be encoded. According to RFC 1738 they are : *)

let special = function 
  | '\000'..'\031' | '\127'..'\255'                      (* non US ASCII *)
  | ' ' | '<' | '>' | '"' | '#' | '%' 
  | '{' | '}' | '|' | '\\' | '^' | '~' | '[' | ']' | '`' (* unsafe *)
  | ';' | '/' | '?' | ':' | '@' | '=' | '&'              (* reserved *)
      -> true
  | _ -> false

let encode s =
  let rec need_code i =
    if i < String.length s then
      match s.[i] with
        ' ' -> true
      | x -> if special x then true else need_code (succ i)
    else false
  in
  let rec compute_len i i1 =
    if i < String.length s then
      let i1 = if special s.[i] then i1 + 3 else succ i1 in
      compute_len (succ i) i1
    else i1
  in
  let rec copy_code_in s1 i i1 =
    if i < String.length s then
      let i1 =
        match s.[i] with
          ' ' -> s1.[i1] <- '+'; succ i1
        | c ->
            if special c then
              begin
                s1.[i1] <- '%';
                s1.[i1 + 1] <- hexa_digit (Char.code c / 16);
                s1.[i1 + 2] <- hexa_digit (Char.code c mod 16);
                i1 + 3
              end
            else begin s1.[i1] <- c; succ i1 end
      in
      copy_code_in s1 (succ i) i1
    else s1
  in
  if need_code 0 then
    let len = compute_len 0 0 in copy_code_in (String.create len) 0 0
  else s
;;

(**********************************************************************)
(* CGI emulation                                                      *)
(**********************************************************************)

let no_actv = lazy (failwith "Outside of CGI environment")

let current_actv = ref (no_actv : Netcgi.cgi Lazy.t)
let current_url = ref ""
let current_resource = ref ""
let current_args = ref []

let parse_args() = !current_args

let real_args actv =
  List.map
    (fun arg ->
       (arg#name, arg#value)
    )
    actv # arguments
;;


let print s =
  let actv = Lazy.force !current_actv in
  actv # output # output_string s
;;


let printf fmt = Printf.ksprintf print fmt


(* content-type *)

let header m =
  let actv = Lazy.force !current_actv in
  actv # set_header
    ~content_type:(if m="" then "text/html" else m)
    ~cache:`No_cache
    ()


(* returns the URL of the CGI *)

let this_url () =
  let _ = Lazy.force !current_actv in
  !current_url

let this_script () =
  let _ = Lazy.force !current_actv in
  !current_resource


(**********************************************************************)
(* CGI registration                                                   *)
(**********************************************************************)

exception Internal_redirect of string * (string * string) list

let cgi_reg = Hashtbl.create 20
let static_reg = Hashtbl.create 100
let init_reg = ref []

let register_cgi ~name f =
  Hashtbl.add cgi_reg name f

let register_static ~name ~content_type ~data =
  Hashtbl.add static_reg name (content_type, data)

let register_init f =
  init_reg := !init_reg @ [f]


let rec invoke_res n resource_name ext_env
                   (actv : Netcgi.cgi_activation) args =
  if n = 0 then
    raise
      (Nethttpd_types.Standard_response
	 (`Internal_server_error, 
	  None, 
	  (Some
	     ("Too many internal redirections to: " ^ resource_name ))));
  List.iter (fun f -> f()) !init_reg;
  try
    let (content_type, data) = Hashtbl.find static_reg resource_name in
    actv # set_header ~content_type();
    actv # output # output_string data;
    actv # output # commit_work()
  with
    | Not_found ->
	( try
	    let f = 
	      try Hashtbl.find cgi_reg resource_name 
	      with
		| Not_found ->
		    raise(Nethttpd_types.Standard_response
			    (`Not_found, None, None))
	    in
	    current_actv := lazy actv;
	    current_url := actv # url();
	    current_resource := resource_name;
            current_args := args;
	    f();
	    actv # output # commit_work()
	  with
	    | Nethttpd_types.Standard_response(_,_,_) as e -> raise e
	    | Internal_redirect(new_script, new_args) ->
		actv # output # rollback_work();
		invoke_res (n-1) new_script ext_env actv new_args

	    | error ->
		raise(Nethttpd_types.Standard_response
			(`Internal_server_error, 
			 None, 
			 (Some
			    ("Uncaught exception: " ^ Printexc.to_string error))))
	)
;;


let invoke_res' n resource_name ext_env
                (actv : Netcgi.cgi_activation) =
  try
    let args = real_args actv in
    invoke_res n resource_name ext_env actv args;
    current_actv := no_actv;
    current_url := "";
    current_resource := "";
    current_args := [];
    List.iter (fun f -> f()) !init_reg;
  with
    | error ->
	actv # output # rollback_work();
	current_actv := no_actv;
	current_url := "";
	current_resource := "";
        current_args := [];
	List.iter (fun f -> f()) !init_reg;
	raise error
;;


let invoke n ext_env (actv : Netcgi.cgi_activation) =
  let resource_name =
    if ext_env#cgi_script_name = "" then
      ""
    else
      String.sub 
	ext_env#cgi_script_name 1 (String.length ext_env#cgi_script_name - 1) in
  let resource_name =
    if resource_name = "" then "index.html" else resource_name in

  invoke_res' n resource_name ext_env actv
;;


let linkdb_handler =
  { Nethttpd_services.dyn_handler = invoke 10;
    dyn_activation = Nethttpd_services.std_activation `Std_activation_buffered;
    dyn_uri = None;
    dyn_translator = (fun _ -> "");
    dyn_accept_all_conditionals = false
  }
;;


let init_config cfg addr =
  let sects = cfg#resolve_section addr "linkdb" in
  match sects with
    | [ sect ] ->
	Config.db_dir := cfg#string_param (cfg#resolve_parameter sect "db_dir");
	Config.log_dir := cfg#string_param (cfg#resolve_parameter sect "log_dir");
    | _ ->
	failwith "linkdb section must only occur once"
;;


let hide_linkdb cfg addr =
object(self)
  method filename = cfg # filename
  method tree = cfg # tree
  method root_addr = cfg # root_addr
  method root_name = cfg # root_name
  method resolve_section = cfg # resolve_section
  method resolve_parameter = cfg # resolve_parameter
  method print = cfg # print
  method string_param = cfg # string_param
  method int_param = cfg # int_param
  method float_param = cfg # float_param
  method bool_param = cfg # bool_param
  method restrict_parameters = cfg # restrict_parameters

  method restrict_subsections a list =
    if a = addr then
      cfg # restrict_subsections a ("linkdb" :: list)
    else
      cfg # restrict_subsections a list

end
;;


let factory() =
  let p =
    Nethttpd_plex.nethttpd_factory
      ~handlers:[ "linkdb", linkdb_handler ]
      () in
  ( object(factory)
      method name = "nethttpd.linkdb"
      method create_processor ctrl_cfg cfg addr =
	init_config cfg addr;
	let cfg' = hide_linkdb cfg addr in
	p # create_processor ctrl_cfg cfg' addr
    end
  )

;;
