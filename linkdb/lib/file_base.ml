(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* File_base: (persistent tables stored in plain files)
 *
 * Stores sequences of records in files. Each record is a set of attributes
 * that are labeled by a 'name' and have a string value. The string value 
 * must not contain newline characters. Each record has an ID that is
 * unique in the whole database (see the Index module).
 *
 * You can read such a file as a whole, select records, change records
 * and write the contents back to a file. 
 *
 * Protection against overlapping concurrent read and/or write operations
 * is guaranteed by locking the whole database using the Lock module.
 * Locks must be obtained and released by the user of this module.
 * There are checks whether there is enough locking.
 *
 * IMPORTANT NOTES:
 * - This module is not well suited for BIG tables as the whole table
 *   must fit into memory and because there is no indexing
 *)

open Common
open Scheme
open Record
open Table

(* Note:
 * The name "_ID_" is reserved 
 *)


(*************** The File_base.t class of databases *****************)


(* Some internally used exceptions *)

exception Next_record;;


type mstate = Init | Unmodified | Modified | Written;;

(* Init:       means a fresh object
 * Unmodified: means that the object is equal to the persistent object
 * Modified:   means that the object is not equal to the persistent object
 * Written:    means that the object has just been written to a file; this
 *             means 'Unmodified' if no transactions are used, otherwise
 *             still 'Modified'
 *)


class t the_scheme the_filename the_table_name =

  (* the_filename: name of a file that is used as storage for the table *)

  object (self)

    inherit Mem_base.t the_scheme as super

    val filename = the_filename
    val table_name = the_table_name

    val mutable old_ids = []
	(* IDs which have been deleted in the meantime *)

    val mutable current_lock = Lock.no_lock

    val mutable modified = Init
    val mutable transaction = false

    method is_modified () = 
      (modified = Modified) or (transaction & modified = Written)
	
    (* File format:
     * - The database is stored as a sequence of records (unordered)
     * - Every record is stored as several lines where each line 
     *   represents an attribute
     * - The line for an attribute is a pair of a name and a value.
     *   The name are all characters before the first ':', the value
     *   are all characters after that ':' until the end of the line
     * - An empty line separates two records.
     * - The special attribute name "_ID_" is used to store the record ID
     * - It is neither possible to use attribute names that contain ':',
     *   nor to store values that contain newline characters.
     *)

    method require_read_lock() =
      Lock.require_read_permission();
      if Lock.current() <> current_lock then
	failwith "File_base # require_read_lock"


    method require_write_lock() =
      Lock.require_write_permission();
      if Lock.current() <> current_lock then
	failwith "File_base # require_write_lock"


    method read_database() =
      (* initialize this object with contents of the file 'filename' *)
      Lock.require_read_permission();
      current_lock <- Lock.current();
      Logging.write Logging.Debug "read_database";
      let ch = open_in_bin (Filename.concat !Config.db_dir filename) in
      let recs = ref [] in
      begin try
	while true do
          (* read next record *)
	  let f = ref [] in
	  begin try
	    while true do
	      (* read next attribute *)
	      let line = input_line ch in
	      if line = "" then
		raise Next_record;
	        (* an empty line separates two records *)
	      let k = 
		try String.index line ':' 
		    with Not_found -> failwith "read_database: line without ':'"
	      in
	      let name = String.sub line 0 k in
	      let value = String.sub line (k+1) (String.length line - k - 1) in
	      if List.mem_assoc name !f then
		failwith ("read_database: double attribute '" ^ name ^ "'");
	      f := (name,value) :: !f
	    done
	  with
	    Next_record -> ()
	  end;
	  if !f <> [] then begin
	    let id = 
	      try int_of_string (List.assoc "_ID_" !f) 
	      with Not_found -> 
		failwith "read_database: record without _ID_"
	      | Failure "int_of_string" -> 
		  failwith "read_database: bad _ID_"
	    in
	    (* interpret 'dbscheme' and lookup the attributes *)
	    let vals =
	      List.map
		(fun (aname, atype) ->
		  let att =
		    try 
		      Field.load_field
			atype
			(List.assoc aname !f) 
		    with 
		      Not_found -> Field.null 
		  in
		  (aname, att))
		(physical_typedef_of_scheme dbscheme)
	    in
	    (* store the ID in the 'id' field, and the other attributes
	     * in the name/value pair list
	     *)
	    let r = new record self dbscheme id vals in
	    recs := r :: !recs;
	  end
	done
      with
        End_of_file -> ()
      | any ->
	  close_in ch;    (* close 'ch' if any exceptions are raised *)
	  raise any
      end;
      close_in ch;
      Logging.write Logging.Debug "Done read database";
      contents <- Array.of_list !recs;
      modified <- Unmodified


    method write_database () =
      (* make the current contents of this object persistent *)
      self # require_write_lock();
      Logging.write Logging.Debug "write_database";
      let alt_filename = filename ^ ".NEW" in
      (* create a new file 'alt_filename' that is later renamed to
       * 'filename'. If the disk becomes full while 'alt_filename' is
       * being written the operation can be canceled without data loss.
       *)

      let ch = open_out (Filename.concat !Config.db_dir alt_filename) in

      let write_record r =
	(* write record 'r' and the separating blank line *)
	output_string ch ("_ID_:" ^ (string_of_int (r#id())) ^ "\n");
	List.iter
	  (fun (name,t) ->
	    let v = r # field name in
	    if v <> Field.null then
	      let s = Field.store_field t v in
	      output_string ch (name ^ ":" ^ s ^ "\n"))
	  (physical_typedef_of_scheme dbscheme);
	output_string ch "\n"
      in

      begin try
	(* write all records *)
	Array.iter write_record contents
      with
	any ->
	  close_out ch;    (* close 'ch' if any exceptions are raised *)
	  raise any
      end;

      close_out ch;

      if not transaction then begin
        (* Now rename 'alt_filename' to 'filename'. *)
	
	let full_alt_filename = Filename.concat !Config.db_dir alt_filename in
	let full_filename = Filename.concat !Config.db_dir filename in

	Sys.rename full_alt_filename full_filename;
      end;

      (* Release IDs of deleted records: *)
      List.iter Index.release_id old_ids;

      modified <- Written;

      ()

    method begin_transaction() =
      if transaction then
	failwith "File_base#begin_transaction: already in transaction";
      if modified = Modified then
	failwith "File_base#begin_transaction: already modified data";

      transaction <- true

    method commit_transaction() =
      if not transaction then
	failwith "File_base#commit_transaction: no transaction";
      if modified = Modified then
	failwith "File_base#commit_transaction: modified but not written data";

      if modified = Written then begin
	let alt_filename = filename ^ ".NEW" in
	let full_alt_filename = Filename.concat !Config.db_dir alt_filename in
	let full_filename = Filename.concat !Config.db_dir filename in
	Sys.rename full_alt_filename full_filename;
	modified <- Unmodified
      end;

      transaction <- false


    method private check_type fields =
      Scheme.check_type dbscheme fields


    method private check_uniqueness r ignore_ids =
      (* r: new or changed record *)
      let unique = unique_attributes dbscheme in
      let td = typedef_of_scheme dbscheme in 
      Array.iter
	(fun r' ->
	  let id = r' # id() in
	  if not(List.mem id ignore_ids) then begin
	    List.iter
	      (fun n ->
		let r_n  = r # field n in
		let r'_n = r' # field n in
		let t = List.assoc n td in
		if r_n <> Field.null & r'_n <> Field.null & 
		  Field.compare_fields t r_n r'_n = 0 then
		  raise (Not_unique n))
	      unique
	  end)
	contents;
      ()


    method private get_derived_field r n =
      Field.null


    method add_record fields =
      (* adds a new record with a new ID to the database. 'fields' is a 
       * list of name/value pairs.
       * The method returns the new ID 
       *)

      if modified = Init then self # read_database();
      self # require_write_lock();
      Logging.write Logging.Info "add_record";
      let new_id = Index.fetch_id table_name in
      Logging.write Logging.Info ("new ID = " ^ string_of_int new_id);
      self # check_type fields;
      let r = new record self dbscheme new_id fields in
      self # check_uniqueness r [];
      contents <- Array.append contents [| r |];
      modified <- Modified;
      new_id


    method change_record id fields =
      (* change the record identified by 'id' to 'fields' *)
      if modified = Init then self # read_database();
      self # require_write_lock();
      Logging.write Logging.Info ("change_record ID=" ^ string_of_int id);
      self # check_type fields;
      modified <- Modified;
      Array.iter
	(fun r -> 
	  if r # id() = id then begin
	    let r' = Oo.copy r in
	    r' # setrec fields;
	    self # check_uniqueness r' [id];
	    r # setrec fields
	  end)
	contents


    method delete_record id =
      (* deletes the record identified by 'id' *)
      if modified = Init then self # read_database();
      self # require_write_lock();
      Logging.write Logging.Info ("delete_record ID=" ^ string_of_int id);
      let rec search k =
	if contents.(k)#id() = id then
	  k
	else
	  search (k+1)
      in
      let k = try search 0 
	  with _ -> failwith "delete_record: ID does not exist"
      in
      let c1 = Array.sub contents 0 k in
      let c2 = Array.sub contents (k+1) (Array.length contents - k - 1) in
      contents <- Array.append c1 c2;
      (* "old_ids": List of IDs to be released in the index when the file
       * is written the next time
       *)
      old_ids <- id :: old_ids;
      modified <- Modified


    method get_record id =
      (* return the record that has the ID 'id'.
       * Raises Not_found if record 'id' does not exist
       *)
      if modified = Init then self # read_database();
      self # require_read_lock();
      super # get_record id


    method get_sorted_records names =
    (* names: a list of attribute names. The records are first sorted by
     * the first attribute (in ascending order), on equality the second
     * attribute is used, and so on.
     *)
      if modified = Init then self # read_database();
      self # require_read_lock();
      super # get_sorted_records names


    method get_somehow_sorted_records names_and_cmp =
      if modified = Init then self # read_database();
      self # require_read_lock();
      super # get_somehow_sorted_records names_and_cmp


    method select pred =
      (* returns all records for which the predicate 'pred' is true *)
      if modified = Init then self # read_database();
      self # require_read_lock();
      super # select pred

  end
