(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Common;;
open Field;;
open Wbuffer;;
open Scheme;;

(**************** fragments for CGI programs ****************)

(* CGI programs almost always output HTML pages that are dynamically
 * generated. It seems not to be a good style to program all generators
 * anew, for example:
 * 
 * print "<HTML><BODY><H1> This is the output </H1>\n";
 * print ("This is the result of my computation: " ^ string_of_t x ^ "\n");
 * print "</BODY></HTML>\n";
 *
 * As you see, most parts of this "dynamic" page are actually static texts
 * that are always the same. It is better to have a separate file, called
 * a "fragment", that contains the static pattern of the page and that
 * has placeholders for dynamic contents.
 * Placeholders are identifiers surrounded by '@' characters. This could
 * be a fragment for the given example:
 * 
 * <HTML><BODY><H1> This is the output </H1>
 * This is the result of my computation: @result@
 * </BODY></HTML>
 *
 * We now need a parser for fragments that can insert values into placeholders.
 * Furthermore, while values are inserted unsafe characters could be protected
 * at the same time (i.e. what the functions escape_html and escape_quotes do).
 * 
 * Some fragments may be output directly as result of the CGI program. 
 * This is not always sufficient. Consider that the result should be
 * formatted using a table. In the main fragment we have the lines:
 * 
 * This is the result of my computation:
 * <table>
 * @table_lines@
 * </table>
 *
 * where for @table_lines@ the concatanation of several representations of
 * table rows should be inserted. Each row may be the result of another
 * fragment, for example:
 *
 * <TR>
 *   <TD>@component1@</TD>
 *   <TD>@component2@</TD>
 * </TR>
 *
 * This means that the second fragment should be instantiated multiple times,
 * the results of this should be concatenated, and this value should be 
 * replaced for @table_lines@ in the first fragment.
 *
 * This demands two functions:
 *  - include_file:           reads a fragment, replaces placeholders and
 *                            outputs the instantiated fragment directly
 *  - include_file_as_string: returns the instantiated fragment as string
 *                            such that it can be used as placeholder value
 *                            in another fragment
 *)
  

type value =
    Verbatim of string          (* put value unchanged into the output stream *)
  | Escape_html of string       (* e.g. replace "<" by "&lt;" *)
  | Escape_quotes of string     (* replace double quote by backslash/quote *)
  | Field_value of Field.field  (* insert value of field *)
;;


let id_re = Str.regexp "@\\([a-zA-Z][a-zA-Z0-9_']*\\)@";;

  (* identifiers are scanned like OCAML identifiers *)


let frag_reg = Hashtbl.create 20


let register_fragment ~name data =
  Hashtbl.add frag_reg name data


let include_file_int print f conf parameters =
  (* internal function: use 'print' as printer for the lines read from the
   * text file 'f' after 'parameters' have been substituted for the placeholders
   *)
  let params' =
    List.map
      (fun (var,value) ->
	match value with
	  Verbatim v -> (var,v)
	| Escape_html v -> (var, escape_html v)
	| Escape_quotes v -> (var, escape_quotes v)
	| Field_value v -> (var, html_output [ Config(conf, [ Field(var,v) ]) ])
      )
      parameters in

  let subst line =
    let var = Str.matched_group 1 line in
    try
      List.assoc var params' 
    with
      Not_found ->
	failwith ("In file " ^ f ^ ": parameter '" ^ var ^ "' not replaced")
          (* It is an error if there are parameters left that are not
           * replaced
           *)
  in

  let data =
    try Hashtbl.find frag_reg f 
    with Not_found -> 
      failwith("Fragment not found: " ^ f) in
  let ch = new Netchannels.input_string data in
  begin try
    while true do
      let line = ch # input_line() in
      let line' = Str.global_substitute id_re subst line in
      print line';
      print "\n";
    done
  with
    End_of_file -> ()
  | any ->
      ch # close_in();
      raise any
  end;
  ch # close_in()
;;


let include_file f conf parameters =
  (* read from file "f" and print it directly to the output buffer after
   * parameter substitution has been performed
   *)
  include_file_int Wbuffer.print f conf parameters
;;


let include_file_as_string f conf parameters =
  (* returns the substituted fragment as string *)
  let s = ref "" in
  let print x = s := !s ^ x in
  include_file_int print f conf parameters;
  !s
;;


let parameters_of_record sch r =
  (* return parameter list of type 'value list' from a record 'r' which
   * is formed following scheme 'sch'
   *)
  let t = typedef_of_scheme sch in
  List.map
    (fun (name, t) -> ("att_" ^ name), Field_value (Record.field r name))
    t
;;


(* Navigators:
 *
 * In CGI applications that do not use HTML frames it is necessary to
 * include a navigation bar that is normally the same on every page.
 * (In an application that does use frames you would normally have
 * a separate frame that is always visible and where you can put
 * hyperlinks or buttons that should be always present.)
 *
 * We assume that the fragment of the navigation bar is stored in the
 * file called navigator (see above for its default value). The navigation 
 * bar is separated from the rest of the page by a horizontal line.
 * Because of this we provide two versions of the bar, one that can be
 * inserted at the top of a page and one that fits best at the bottom of
 * a page.
 *
 * Note: A navigation bar is only created if there is a CGI parameter
 * (either classical or automatic) "frames" with the value "true".
 *)


let top_navigator_as_string() =
  if any_param "frames" <> "true" then
    include_file_as_string navigator [] [] ^ "<hr>\n"
  else 
    ""
;;


let bottom_navigator_as_string() =
  if any_param "frames" <> "true" then
    "<hr>\n" ^ include_file_as_string navigator [] []
  else 
    ""
;;


let bottom_navigator() =
  print (bottom_navigator_as_string())
;;


let top_navigator() =
  print (top_navigator_as_string())
;;


(* The following function executes the function 'f' passed as argument
 * and catches exceptions. 
 * If no exception occurs, all what has been printed into the Wbuffer is 
 * committed and actually written to stdout. Otherwise, the Wbuffer is 
 * discarded, i.e. all output from f() is deleted as it is assumed that the 
 * exception has been raised in the middle of printing something into the
 * Wbuffer. (If the Wbuffer would not be discarded, there would be the danger
 * that a half-built page is sent to the browser, and even worse, we would
 * have to append an error message to such a page.)
 * In the case of an exception condition, an error message is formed.
 * User_error exceptions and other exceptions are distinguished (see above
 * for note about User_error).
 *)

let do_protected f =
  (* run f() and do a cgi_commit on success. Otherwise, discard the output
   * buffer with cgi_rollback and generate an error message.
   * Note: The CGI header must already have been written.
   *
   * Note: 'f' is executed with an active read lock; i.e. every program 
   * using do_protected has automatically a read lock.
   *)
  try 
    Lock.get_read_lock();
    f();
    Lock.release_read_lock();
      (* Note: In the case of an exception we do not release the read lock.
       * The read lock is automatically released if the process finishes.
       *)
    cgi_commit();
    Logging.write Logging.Info "OK"
  with
  | Cgi.Internal_redirect(_,_) as e ->
      raise e
  | User_error s ->
      cgi_rollback();
      print "<html>\n";
      print "<head><title>User error</title></head>\n";
      print "<body bgcolor=white>\n";
      print "<br><br>\n";
      print "<table><tr><td width=30>&nbsp;</td><td>";
      print "<h1>Sorry,</h1>\n";
      print "the system could not respond to your request:<br><br>\n";
      print "<table cellspacing=0 cellpadding=0 border=0>\n";
      print "<tr>\n";
      print "<td bgcolor=red width=3>&nbsp;</td>\n";
      print "<td width=8>&nbsp;</td>\n";
      print "<td>";
      print s;
      print "</td>\n";
      print "</tr>\n";
      print "</table>\n";
      print (back_button "Correct your error");
      print "</td></tr></table>";

      bottom_navigator();
      print "</body>\n</html>\n";
      cgi_commit();
      Logging.write Logging.Info ("User_error: " ^ s)
  | any ->
      let s = Printexc.to_string any in
      cgi_rollback();
      print "<html>\n";
      print "<head><title>System error</title></head>\n";
      print "<body bgcolor=white>\n";
      print "<br><br>\n";
      print "<table><tr><td width=30>&nbsp;</td><td>";
      print "<h1>Software error</h1>\n";
      print "<p> Sorry, the last action did not succeed because ";
      print "of an error in the server software.\n";
      print "<p> Please report this bug to ";
      print "<a href=\"";
      print webmaster;
      print "\">me</a>, I'll do my best to repair the system.\n";
      print "<p> Please include in your bug report a description of your last action ";
      print "and the following error message:\n";
      print "<p><b>";
      print (escape_html s);
      print "</b>\n";
      print "</td></tr></table>";
      bottom_navigator();
      print "</body>\n</html>\n";
      cgi_commit();
      Logging.write Logging.Err ("System error: " ^ s)
;;


(* Print a header. The "Pragma" line is for older browsers and proxies that
 * cannot recognize dynamic pages otherwise. (Modern browsers do this by
 * the non-existence of some header fields that must only be present if 
 * the page is static. These fields are usually "Date" and "Expiration".
 *)

let html_header() = Cgi.header ""
;;
