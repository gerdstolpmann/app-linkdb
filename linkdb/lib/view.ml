(* $Id$
 * ----------------------------------------------------------------------
 *
 *)


class intersection base_tables =
  (* This implementation is VERY slow *)

  let the_scheme =
    Scheme.intersection (List.map (fun t -> t # scheme) base_tables) in
  object (self)
    inherit Mem_base.t the_scheme as super

    val tables = base_tables

    method read_database () =
      let rec readtab tabs =
	match tabs with
	  [] -> []
	| tab::tabs' ->
	    let recs  = tab # select (fun r -> true) in
	    let recs' = readtab tabs' in
	    recs @ recs'
      in
      contents <- Array.of_list (readtab tables)

    method write_database () =
      failwith "View.intersection # write_database: not possible"

    method begin_transaction() = ()
    method commit_transaction() = ()

    method is_modified () = false

    method get_derived_field r name =
      failwith "View.intersection # get_derived_field: not possible"

    method add_record r =
      failwith "View.intersection # add_record: not possible"

    method change_record id r =
      failwith "View.intersection # change_record: not possible"

    method delete_record id =
      failwith "View.intersection # delete_record: not possible"

    method get_record n =
      self # read_database();
      super # get_record n

    method get_sorted_records flist =
      self # read_database();
      super # get_sorted_records flist

    method get_somehow_sorted_records spec =
      self # read_database();
      super # get_somehow_sorted_records spec

    method select selector =
      self # read_database();
      super # select selector
  end
;;
