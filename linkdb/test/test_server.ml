(* $Id$ *)


let start() =
  let (opt_list, cmdline_cfg) = Netplex_main.args() in

  let opt_list' =
    opt_list in

  Arg.parse
    opt_list'
    (fun s -> raise (Arg.Bad ("Don't know what to do with: " ^ s)))
    "usage: test_server [options]";
  let parallelizer =
    Netplex_mp.mp() in  (* multi-processing *)
  Netplex_main.startup
    parallelizer
    Netplex_log.logger_factories
    Netplex_workload.workload_manager_factories
    [ App_linkdb.Lib.Cgi.factory();
      Nethttpd_plex.nethttpd_factory()
    ]
    cmdline_cfg
;;

Sys.set_signal Sys.sigpipe Sys.Signal_ignore;
start();;
